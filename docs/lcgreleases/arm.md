# State of the ARM platform in the LCG Releases

The toolchain for ARM is integrated into the nightly builds as the `aarch64-el9-gcc13-opt` platform.
For example dev4 in CVMFS here `/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/dev4/aarch64-el9-gcc13-opt`.

## Requirements

* `aarch64` machine

* Docker or an AlmaLinux 9 operating system with the necessary build requirements installed, similar to the
  content of hep_osLibs for x86_64. Docker containers can be found in the [sft/docker gitlab
  registry](https://gitlab-registry.cern.ch/sft/docker/alma9-core:aarch64)

* For example the gcc compiler from the LCG contrib area `/cvmfs/sft.cern.ch/lcg/contrib/gcc/13/aarch64-el9/`

## Environment setup for building

* `lcgcmake/jenkins/jk-setup.sh Release gcc13`

## Limitations

Most packages are also compiled on aarch64, please see [ARM on lcginfo](https://lcginfo.cern.ch/platform/aarch64-el9-gcc13-opt/) for the list of packages.
