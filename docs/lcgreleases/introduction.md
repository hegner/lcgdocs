LCG RELEASES
============

Welcome to the LCG Releases provided by the SPI team in EP-SFT at CERN.

In the CVMFS repository `/cvmfs/sft.cern.ch` you can find a software stack containing over 800 external 
packages as well as HEP specific tools and generators. There are usually two releases per year as well
as development builds every night (except Sundays).

The releases start with the prefix `LCG_` followed by the major version of the release, e.g. `101`. A
major release implies major version changes in all packages of the software stack. For patches, we
append lowercase letters like `a`, `b`, ... to the name of the release.

**Example:** The release 101 could provide the following version:

- A release candidate used for testing an upcoming major release: `LCG_101rc1`
- A new major release: `LCG_101`
- A patch release (only if necessary) to fix problems in this major release: `LCG_101a`

For the Nightlies we provide the following general configurations:

- `dev4` Based on the latest stable version branch of ROOT and Python 3.11
- `dev3` Based on the latest git HEAD of ROOT and Python 3.11 (called 'Bleeding Edge' in SWAN)

and a few specific ones

- `devswan` Based on `dev3` with a set of [SWAN specific additions](https://gitlab.cern.ch/sft/lcgcmake/-/raw/master/cmake/toolchain/heptools-devswan.cmake)
- `dev{3,4}cuda` Versions of `dev{3,4}` with CUDA support
- `devgeantv`, `devAdePT` Optimised for simulation R&D
- `devnxcals` dev with [NXCALS specific packages](https://gitlab.cern.ch/sft/lcgcmake/-/raw/master/cmake/toolchain/heptools-devnxcals.cmake)


Prerequisites
-------------

The philosophy is to try to use as many packages from the operating system as possible. A curated list of
packages for the supported operating systems (currently Alma 8 and Alma 9 (or equivalent RHEL rebuilds), and Ubuntu 20.04 and 22.04 and 24.04)
exists and goes under the name HEP_OSlibs.
HEP_OSlibs contains the minimal set of packages required on a machine in order for the LCG releases to work.
More information can be found at [CERN's GitLab](https://gitlab.cern.ch/linuxsupport/rpms/HEP_OSlibs).

Setup
-----

For most of our distributed software you can find both a Bash script (ending in `.sh`) as well as a
C Shell script (ending in `.csh`). By using the `source` command in your local shell with these
scripts you can change your current shell environment to use the software on CVMFS instead of your
locally installed packages. These changes only last until you close the current terminal session.

In the following sections we introduce the most used features of the LCG Releases in `/cvmfs/sft.cern.ch/lcg/`:

###  `contrib`: compilers, cmake, et al.
-   Used for compilers, CMake and other build tools.
-   We provide and maintain `gcc`, `clang`, `CMake`, and `cuda`. The compilers come bundled with `binutils`.
-   The other subfolders in `contrib` are not as well maintained as the four mentioned above.

!!! example
    Use the latest stable GCC 14 on a Almalinux 9 machine in a Bash shell:
    `source /cvmfs/sft.cern.ch/lcg/contrib/gcc/14/x86_64-el9/setup.sh`

###  Complete stacks: `views`
-   You can make all 800+ packages of the LCG software stack available in your current shell without any installations.
-   The setup takes a couple of seconds in the CERN network. Outside it might take a bit longer.
-   For the nightly builds, there's a `latest` symlink that links to the latest stable build.

!!! example
    Use the LCG release 106 built with GCC 13 on a Almalinux 9 machine in a Bash shell:
    `source /cvmfs/sft.cern.ch/lcg/views/LCG_106/x86_64-el9-gcc13-opt/setup.sh`

!!! example
    Use the Nightly *dev4* built with Clang 16 on a Almalinux 9 machine in a Bash shell:
    `source /cvmfs/sft.cern.ch/lcg/views/dev4/latest/x86_64-el9-clang16-opt/setup.sh`

### Individual packages: `releases`
-   If you only need a specific package (including its dependencies) you can also do that.

!!! example
     Use only `Geant4` from the LCG_106 release for Almalinux 9 and Clang 16 in a Bash shell:
    `source /cvmfs/sft.cern.ch/lcg/releases/LCG_106/Geant4/11.2.1/x86_64-el9-gcc13-opt/Geant4-env.sh`

!!! warning
    Note that the package in the example above is inside of a valid release directory (`LCG_106/`).
    Packages versions inside of `releases/<pgp_name>`, often denoted by a hash value at the end of their name,
    are not intended for external use and may not work. Their use is discouraged. In other words,
    use `/cvmfs/sft.cern.ch/lcg/releases/LCG_97/GSL/2.5/x86_64-centos7-gcc9-opt/GSL-env.sh`
    instead of `/cvmfs/sft.cern.ch/lcg/releases/GSL/2.5-32fc5/x86_64-centos7-gcc9-opt/GSL-env.sh`.

###  `nightlies`
-   Same as `releases` for the nightly builds: Use single packages instead of an entire view
    
    !!! example
        Use only `RELAX` from Friday's dev4 nightly for Almalinux 9 and GCC 14 in a Bash shell:
        `source /cvmfs/sft.cern.ch/lcg/nightlies/dev4/Fri/RELAX/6.1.2/x86_64-el9-gcc13-opt/RELAX-env.sh`

Docker Containers
-----------------

Docker containers that are expected to be compatible with the LCG views provided via CVMFS can be found in the
[sft/docker gitlab container
registry](https://gitlab.cern.ch/sft/docker/container_registry?orderBy=UPDATED&sort=desc&search[]=-core&search[]=). The
containers ending with `-core` are meant to be used

* gitlab-registry.cern.ch/sft/docker/centos7-core
* gitlab-registry.cern.ch/sft/docker/alma8-core
* gitlab-registry.cern.ch/sft/docker/alma9-core

These containers are based on the containers provided by linux support and only the HEP_OSlibs package is installed on
top of them.

There will be a tagged version of the container with the date it was created, and a latest tag. For AlmaLinux 9 and
CentOS 7 a container for the `aarch64` architecture is provided as well. For this architecture the latest container is
under the `aarch64` tag.

For example:

For x86_64:

* gitlab-registry.cern.ch/sft/docker/alma9-core:latest
* gitlab-registry.cern.ch/sft/docker/alma9-core:2023-10-26

For aarch64:

* gitlab-registry.cern.ch/sft/docker/alma9-core:aarch64
* gitlab-registry.cern.ch/sft/docker/alma9-core:2023-10-26_aarch64

Support Channels
----------------

If you experience any issues while using our software stack, please don't hesitate to
[raise a JIRA issue](https://sft.its.cern.ch/jira/projects/SPI)
or [write an email to the SPI team](mailto:ep-sft-spi@cern.ch)

* You can receive notifications about closed/resolved jira issues by subscribing to the
  [lcgstack-jira-notifications](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=lcgstack-jira-notifications) e-group

Further Information
-------------------

- Lookup all supported packages, releases and platforms: [LCG Info](http://lcginfo.cern.ch/)
- Our internal documentation: [LCG Docs](http://cern.ch/lcgdocs)
- The [LCG CMake project on GitLab](https://gitlab.cern.ch/sft/lcgcmake)
- The website of the [SPI team](http://cern.ch/spi) providing links to all offered services

File System Layout
------------------

```txt
/cvmfs/sft.cern.ch/lcg/
    releases/
        <package>/ # This is the place where the actual package files are living
            <version>-<hash>/         # hex hash value calculated from package dependencies
                <platform>/           # platform tag <architecture>-<osversion>-<compiler>-<buuldtype>
                    <package>-env.sh  # script to set the environment
                    logs/             # log files generated during the build process
                    include/
                    lib/
                    bin/
                    ...
        <package2>
            ...
        LCG<XX>/ # Collection of all packages, versions and hashes that were used in this release
            <package>/
                <version>/
                    <platform> # link to ../../../<package>/<version>-<hash>/<platform>

        LCG_<YY>/
            ...
        ...
    views/
        dev<AA>/
            latest/
                <platform>/ # link to latest stable ../<weekday>/<platform>
            <weekday>/
                <platform>/
                    setup.csh
                    setup.sh
                    bin/
                        <binaryA> # link to previous release /cvmfs/sft.cern.ch/lcg/releases/<package>/<version>-<hash>/<platform>/bin/<binaryA>
                        <binaryB> # link nightly build (increment) -> /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/dev<AA>/<weekday>/<package>/<version>/<platform>/bin/<binaryB>
                        ...
                    include/
                        <includeA> # link to previous release /cvmfs/sft.cern.ch/lcg/releases/<package>/<version>-<hash>/<platform>/include/<includeA>
                        <includeB> # link nightly build (increment) -> /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/dev<AA>/<weekday>/<package>/<version>/<platform>/include/<includeB>
                        ...
                    lib/
                        <libraryA> # link to previous release /cvmfs/sft.cern.ch/lcg/releases/<package>/<version>-<hash>/<platform>/lib/<libraryA>
                        <libraryB> # link nightly build (increment) -> /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/dev<AA>/<weekday>/<package>/<version>/<platform>/lib/<libraryB>
                    lib64/
                    ...
                ...
            Tue/
            Wed/
            Thu/
            ...
        dev<AA>python3/
        dev<BB>/
        LCG<XX>/
            <platform>/
                setup.csh
                setup.sh
                bin/
                    <binary> # link to release /sft.cern.ch/lcg/releases/<package>/<version>-<hash>/<platform>/bin/<binary>
                    ...
                include/
                    <include> # link to release /cvmfs/sft.cern.ch/lcg/releases/<package>/<version>-<hash>/<platform>/include/<include>
                    ...
                lib/
                    <library> # link to release /sft.cern.ch/lcg/releases/<package>/<version>-<hash>/<platform>/lib/<library>
                    ...
                lib64/
                ...
            ...
        LCG<XX>python3/
        LCG<YY>/
        ...
    contrib/
        CMake/
            latest # link to latest stable version, e.g ./3.13.4
            3.13.4/
                Linux-x86_64/
                    bin/
                        cmake
                        ctest
                        ...
                    ...
            ...
        gcc/
            8         # link to stable version ./8.2.0
            8binutils # link to stable version ./8.2.0
            8testing  # link to experimental version ./8.3.0
            8.2.0/
                x86_64-centos7 # link to /cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-<hash>/x86_64-centos7/
                x86_64-slc6 # link to /cvmfs/sft.cern.ch/lcg/releases/gcc/8.2.0-<hash>/x86_64-slc6/
                    setup.csh # Setup script for this gcc compiler in C shell
                    setup.sh # Setup script for this gcc compiler in bash
                    bin/
                    ...
            ...
        clang/
            8         # link to stable version ./8.0.0
            8binutils # link to stable version ./8.0.0
            8.0.0/
                x86_64-centos7 # link to /cvmfs/sft.cern.ch/lcg/releases/clang/8.0.0-<hash>/x86_64-centos7/
                    setup.csh # Setup script for this clang compiler in C shell
                    setup.sh # Setup script for this clang compiler in bash
                    bin/
                    ...
                x86_64-slc6 # link to /cvmfs/sft.cern.ch/lcg/releases/clang/8.0.0-<hash>/x86_64-slc6/
        ...
```

Alternatives
------------

If you don't want to use the the LCG software stack in a Bash environment, we also provide other
pre-built distributions:

* Tarballs, accessible via
    * XRootD: `root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/releases/`
    * EOS Fuse client: `/eos/project/l/lcg/www/lcgpackages/tarFiles/releases/`
    * HTTP `https://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases`
* RPMs
    * We provide a repository for the `yum` package manager on CentOS 7, AlmaLinux 8 and AlmaLinux 9 (or equivalent)
    * You can find the configuration for `yum` online at [https://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/](https://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/). For example :
        * [`lcg7.repo`](https://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/yum.repos.d/lcg7.repo) (also reported below)
        * [`lcg7debug.repo`](https://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/yum.repos.d/lcg7debug.repo)
    * Legacy repos files are available from the same directory ([`lcglegacy.repo`](https://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/yum.repos.d/lcglegacy.repo) and [`lcg-testlegacy.repo`](https://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/yum.repos.d/lcg-testlegacy.repo)).
```ini
[lcg7102]
name=LCG 102 Releases
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/x86_64/LCG_102/
gpgcheck=0
enabled=1
protect=0

[lcg7packs]
name=LCG Release Packages
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/x86_64/Packages/
gpgcheck=0
enabled=1
protect=0

[lcg7102-aarch64]
name=LCG 102 Releases
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/aarch64/LCG_102/
gpgcheck=0
enabled=1
protect=0

[lcg7packs-aarch64]
name=LCG Release aarch64 Packages
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/aarch64/Packages/
gpgcheck=0
enabled=1
protect=0

[lcg-contrib]
name=LCG contrib (gcc, clang, binutils, ...)
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/contrib/7/
gpgcheck=0
enabled=1
protect=0
```

    
