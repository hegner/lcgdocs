## New layout for the LCG RPM repository

Starting with LCG_102 a new layout has been put in place with the aim to i) remove duplication of packages across releases, ii) reduce the size of single repositories by a better partitioning, in particular wrt debug builds; iii) restore functionaity of the REVISION field in the RPMs.

YUM repo files for the LCG_102 @ CentOS7 can be found at [lcg7.repo](http://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/yum.repos.d/lcg7.repo) and [lcg7debug.repo](http://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/yum.repos.d/lcg7debug.repo) .
RPMs are also available for Alma Linux 9.

## Previous layout for the LCG RPM repository

**NB: Applies to LCG_97 to LCG_101**

Starting with release LCG_97, we adopt a new layout for the LCG RPMs.
The repository will still be available from http://lcgpackages.web.cern.ch/lcgpackages. 
The existing repositories

       <url>/lcgpackages/rpms
       <url>/lcgpackages/rpms_updates
       <url>/lcgpackages/rpms_contrib
       <url>/lcgpackages/rpms_test

will remain available for legacy and complement (some needed RPMs can be still available form them).

The new layout is in the form

    <url>/lcgpackages/lcg/<repo>/<OSn>/LCG_<version>
    
with <repo> taking the following values: 'repo', 'testing'; <OSn> refering to the reference RHEL release,
e.g. '7' for CentOS7, '8' for CentOS8, etc. ; <version> to the LCG version, e.g. '97' for LCG_97.

Examples of yum.repos.d config file can be found at http://lcgpackages.web.cern.ch/lcgpackages/lcg/etc/yum.repos.d/lcg.repo . 

The new layout for CentOS7, LCG_97, 'repo' will look like:

    <url>/lcgpackages/lcg/repo/7/LCG_97
 
The real, package-hash, RPMs go under /Packages

    <url>/lcgpackages/lcg/repo/7/LCG_97/Packages

and the RPM database under /repodata

    <url>/lcgpackages/lcg/repo/7/LCG_97/repodata

Meta or link RPMS will go into dedicated subdirs

        <url>/lcgpackages/lcg/repo/7/LCG_97/LCG_97/
        <url>/lcgpackages/lcg/repo/7/LCG_97/LCG_97python3/
        <url>/lcgpackages/lcg/repo/7/LCG_97/LCG_97_ATLAS_1
        <url>/lcgpackages/lcg/repo/7/LCG_97/LCG_97_LHCB_1
 
This schema allows also use with the YUM0…YUM9 variables.

There will be also a new

    <url>/lcgpackages/lcg/contrib/7/

for 'contrib' like RPMs.
