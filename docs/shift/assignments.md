# SPI Shift: Assignments

## [Report log](https://codimd.web.cern.ch/SQDUCsLPRvGu6BpeBar9vw?view#)

## 2025

| WEEK                    | PERSON              | 
|:------------------------|:--------------------|
| 2025-02-21 ‒ 2025-02-27 | Nils Langguth       |
| 2025-02-14 ‒ 2025-02-20 | Andre Sailer        |
| 2025-02-07 ‒ 2025-02-13 | Ilias Goulas        |
| 2025-01-31 ‒ 2025-02-06 | Dmitri Konstantinov |
| 2025-01-24 ‒ 2025-01-30 | Nils Langguth       |
| 2025-01-17 ‒ 2025-01-23 | Andre Sailer        |
| 2025-01-10 ‒ 2025-01-16 | Ilias Goulas        |
| 2025-01-06 ‒ 2025-01-09 | Dmitri Konstantinov |


## 2024
| WEEK                    | PERSON              | 
|:------------------------|:--------------------|
| 2024-12-13 ‒ 2024-12-19 | Dmitri Konstantinov |
| 2024-12-06 ‒ 2024-12-12 | Andre Sailer        |
| 2024-11-29 ‒ 2024-12-05 | Ilias Goulas        |
| 2024-11-22 ‒ 2024-11-28 | Dmitri Konstantinov |
| 2024-11-15 ‒ 2024-11-21 | Andre Sailer        |
| 2024-11-08 ‒ 2024-11-14 | Ilias Goulas        |
| 2024-11-01 ‒ 2024-11-07 | Dmitri Konstantinov |
| 2024-10-25 ‒ 2024-10-31 | Andre Sailer        |
| 2024-10-18 ‒ 2024-10-24 | Ilias Goulas        |
| 2024-10-11 ‒ 2024-10-17 | Dmitri Konstantinov |
| 2024-10-04 ‒ 2024-10-10 | Andre Sailer        |
| 2024-09-27 ‒ 2024-10-03 | Ilias Goulas        |
| 2024-09-20 ‒ 2024-09-26 | Dmitri Konstantinov |
| 2024-09-13 ‒ 2024-09-19 | Andre Sailer        |
| 2024-09-06 ‒ 2024-09-12 | Ilias Goulas        |
| 2024-08-30 ‒ 2024-09-05 | Dmitri Konstantinov |
| 2024-08-23 ‒ 2024-08-29 | Andre Sailer        |
| 2024-08-16 ‒ 2024-08-22 | Ilias Goulas        |
| 2024-08-09 ‒ 2024-08-15 | Tim Ehmann          |
| 2024-08-02 ‒ 2024-08-08 | Andre Sailer        |
| 2024-07-26 ‒ 2024-08-01 | Ilias Goulas        |
| 2024-07-19 ‒ 2024-07-25 | Dmitri Konstantinov |
| 2024-07-12 ‒ 2024-07-18 | Tim Ehmann          |
| 2024-07-05 ‒ 2024-07-11 | Andre Sailer        |
| 2024-06-28 ‒ 2024-07-04 | Ilias Goulas        |
| 2024-06-21 ‒ 2024-06-27 | Dmitri Konstantinov |
| 2024-06-14 ‒ 2024-06-20 | Tim Ehmann          |
| 2024-06-07 ‒ 2024-06-13 | Andre Sailer        |
| 2024-05-31 ‒ 2024-06-06 | Ilias Goulas        |
| 2024-05-24 ‒ 2024-05-30 | Dmitri Konstantinov |
| 2024-05-17 ‒ 2024-06-23 | Tim Ehmann          |
| 2024-05-10 ‒ 2024-05-16 | Andre Sailer        |
| 2024-05-03 ‒ 2024-05-09 | Ilias Goulas        |
| 2024-04-26 ‒ 2024-05-02 | Dmitri Konstantinov |
| 2024-04-19 ‒ 2024-04-25 | Tim Ehmann          |
| 2024-04-12 ‒ 2024-04-18 | Andre Sailer        |
| 2024-04-05 ‒ 2024-04-11 | Ilias Goulas        |
| 2024-03-29 ‒ 2024-04-04 | Dmitri Konstantinov |
| 2024-03-22 ‒ 2024-03-28 | Tim Ehmann          |
| 2024-03-15 ‒ 2024-03-21 | Andre Sailer        |
| 2024-03-08 ‒ 2024-03-14 | Ilias Goulas        |
| 2024-03-01 ‒ 2024-03-07 | Andre Sailer        |
| 2024-02-23 ‒ 2024-02-29 | Ilias Goulas        |
| 2024-02-16 ‒ 2024-02-22 | Andre Sailer        |
| 2024-02-09 ‒ 2024-02-15 | Dmitri Konstantinov |
| 2024-02-02 ‒ 2024-02-08 | Tim Ehmann          |
| 2024-01-26 ‒ 2024-02-01 | Ilias Goulas        |
| 2024-01-19 ‒ 2024-01-25 | Andre Sailer        |
| 2024-01-12 ‒ 2024-01-18 | Dmitri Konstantinov |
| 2024-01-08 ‒ 2024-01-11 | Tim Ehmann          |


## 2023
| WEEK                    | PERSON              |    REPORT    |
|:------------------------|:--------------------|--------------|
| 2023-12-15 ‒ 2023-12-21 | Andre Sailer        |              |
| 2023-12-08 ‒ 2023-12-14 | Dmitri Konstantinov |              |
| 2023-12-01 ‒ 2023-12-07 | Tim Ehmann          |              |
| 2023-11-24 ‒ 2023-11-30 | Ilias Goulas        |              |
| 2023-11-17 ‒ 2023-11-23 | Andre Sailer        |              |
| 2023-11-10 ‒ 2023-11-16 | Dmitri Konstantinov |              |
| 2023-11-03 ‒ 2023-11-09 | Tim Ehmann          |              |
| 2023-10-27 ‒ 2023-11-02 | Andre Sailer        |              |
| 2023-10-20 ‒ 2023-10-26 | Ilias Goulas        |              |
| 2023-10-13 ‒ 2023-10-19 | Dmitri Konstantinov |              |
| 2023-10-06 ‒ 2023-10-12 | Tim Ehmann          |              |
| 2023-09-29 ‒ 2023-10-05 | Andre Sailer        |              |
| 2023-09-22 ‒ 2023-09-28 | Ilias Goulas        |              |
| 2023-09-15 ‒ 2023-09-21 | Tim Ehmann          |              |
| 2023-09-08 ‒ 2023-09-14 | Dmitri Konstantinov |              |
| 2023-09-01 ‒ 2023-09-07 | Andre Sailer        | [report](https://codimd.web.cern.ch/a1J2nOLNSQS2V1cQOhNGuw?view#September-1-%E2%80%93-September-7)              |
| 2023-08-25 ‒ 2023-08-31 | Ilias Goulas        |              |
| 2023-08-18 ‒ 2023-08-24 | Andre Sailer        |              |
| 2023-08-11 ‒ 2023-08-17 | Dmitri Konstantinov |              |
| 2023-08-04 ‒ 2023-08-10 | Dmitri Konstantinov |              |
| 2023-07-28 ‒ 2023-08-03 | Andre Sailer        |              |
| 2023-07-21 ‒ 2023-07-27 | Ilias Goulas        |              |
| 2023-07-14 ‒ 2023-07-20 | Ole Morud           |              |
| 2023-07-07 ‒ 2023-07-13 | Andre Sailer | [report](https://codimd.web.cern.ch/a1J2nOLNSQS2V1cQOhNGuw?view#July-7-%E2%80%93-July-13)             |
| 2023-06-30 ‒ 2023-07-06 | Dmitri Konstantinov |              |
| 2023-06-23 ‒ 2023-06-29 | Ilias Goulas        |              |
| 2023-06-16 ‒ 2023-06-22 | Andre Sailer        |              |
| 2023-06-09 ‒ 2023-06-15 | Ole Morud           |              |
| 2023-06-02 ‒ 2023-06-08 | Dmitri Konstantinov |              |
| 2023-05-26 ‒ 2023-06-01 | Ilias Goulas        |              |
| 2023-05-19 ‒ 2023-05-25 | Andre Sailer        |              |
| 2023-05-12 ‒ 2023-05-18 | Ole Morud           |              |
| 2023-05-05 ‒ 2023-05-11 | Dmitri Konstantinov |              |
| 2023-04-28 ‒ 2023-05-04 | Ilias Goulas        |              |
| 2023-04-21 ‒ 2023-04-27 | Andre Sailer        |              |
| 2023-04-14 ‒ 2023-04-20 | Ole Morud           |              |
| 2023-04-07 ‒ 2023-04-13 | Dmitri Konstantinov |              |
| 2023-03-31 ‒ 2023-04-06 | Gerardo Ganis       |              |
| 2023-03-24 ‒ 2023-03-30 | Ilias Goulas        |              |
| 2023-03-17 ‒ 2023-03-23 | Andre Sailer        | [report](https://codimd.web.cern.ch/a1J2nOLNSQS2V1cQOhNGuw?view#March-17-%E2%80%93-March-23)             |
| 2023-03-10 ‒ 2023-03-16 | Ole Morud           |              |
| 2023-03-03 ‒ 2023-03-09 | Dmitri Konstantinov |              |
| 2023-02-24 ‒ 2023-03-02 | Ilias Goulas        |              |
| 2023-02-17 ‒ 2023-02-23 | Andre Sailer        |              |
| 2023-02-10 ‒ 2023-02-16 | Ole Morud           |              |
| 2023-02-03 ‒ 2023-02-09 | Dmitri Konstantinov |              |
| 2023-01-27 ‒ 2023-02-02 | Ilias Goulas        |  [report](https://codimd.web.cern.ch/s/-bqXPS8VS)          | 
| 2023-01-20 ‒ 2023-01-26 | Andre Sailer        |              |
| 2023-01-13 ‒ 2023-01-19 | Gerardo Ganis       |              |
| 2023-01-06 ‒ 2023-01-12 | Ole Morud           |              |

## 2022
| WEEK                    | PERSON              |    REPORT    |
|:------------------------|:--------------------|--------------|
| 2022-12-16 ‒ 2022-12-22 | Dmitri Konstantinov |              |
| 2022-12-09 ‒ 2022-12-15 | Ilias Goulas        |  [report](https://codimd.web.cern.ch/oV85YhXMTPq1ITCexABPGQ)          | 
| 2022-12-02 ‒ 2022-12-08 | Andre Sailer        |  [report](https://codimd.web.cern.ch/Pf4qkdMdQMOg_KAmQDmxQw?view#Shift-from-December-2-to-December-8)            |
| 2022-11-25 ‒ 2022-12-01 | Gerardo Ganis       | [report](https://codimd.web.cern.ch/7w5HlUsRSoiuq-zmJR7p7Q)             |
| 2022-11-18 ‒ 2022-11-24 | Ole Morud           |              |
| 2022-11-11 ‒ 2022-11-17 | Dmitri Konstantinov |              |
| 2022-11-04 ‒ 2022-11-10 | Ilias Goulas        |              | 
| 2022-10-28 ‒ 2022-11-03 | Andre Sailer        | [report](https://codimd.web.cern.ch/Pf4qkdMdQMOg_KAmQDmxQw?view#Shift-from-October-28-to-November-3)             |
| 2022-10-21 ‒ 2022-10-27 | Gerardo Ganis       |   [report](https://codimd.web.cern.ch/s/wQrJXcc6O)           |
| 2022-10-14 ‒ 2022-10-20 | Ole Morud   |              |
| 2022-10-07 ‒ 2022-10-13 | Dmitri Konstantinov |              |
| 2022-09-30 ‒ 2022-10-06 | Ilias Goulas        | [report](https://codimd.web.cern.ch/s/K_FVF8ypC) | 
| 2022-09-23 ‒ 2022-09-29 | Andre Sailer        |              |
| 2022-09-16 ‒ 2022-09-22 | Gerardo Ganis       |              |
| 2022-09-09 ‒ 2022-09-15 | Ole Morud           |              |
| 2022-09-02 ‒ 2022-09-08 | Dmitri Konstantinov |              |
| 2022-08-26 ‒ 2022-09-01 | Ilias Goulas        | [report](https://codimd.web.cern.ch/s/wIOddaJwR) |
| 2022-08-19 ‒ 2022-08-25 | Andre Sailer        |              |
| 2022-08-12 ‒ 2022-08-18 | Ilias Goulas        | [report](https://codimd.web.cern.ch/s/TWemGXbXe) |
| 2022-08-05 ‒ 2022-08-11 | Dmitri Konstantinov |              |
| 2022-07-29 ‒ 2022-08-04 | Harald Hansen       |  [report](https://codimd.web.cern.ch/s/B-J_KvyKB#)            |
| 2022-07-22 ‒ 2022-07-28 | Gerardo Ganis        |  [report](https://codimd.web.cern.ch/Z1pb6pWlQTKrSuY4-IWOFQ)            | 
| 2022-07-15 ‒ 2022-07-21 | Andre Sailer        |  [report](https://codimd.web.cern.ch/Pf4qkdMdQMOg_KAmQDmxQw?view#Shift-from-July-15-to-July-21)            |
| 2022-07-08 ‒ 2022-07-14 | Dmitri Konstantinov      |              |
| 2022-07-01 ‒ 2022-07-07 | Harald Hansen       | [report](https://codimd.web.cern.ch/s/xMSs_XyVf#)             |
| 2022-06-24 ‒ 2022-06-30 | Gerardo Ganis |  [report](https://codimd.web.cern.ch/Ji5KfYZsSt6Ibj6AKNZdZA)            |
| 2022-06-17 ‒ 2022-06-23 | Ilias Goulas        | [report](https://codimd.web.cern.ch/s/74t06QrkP)             | 
| 2022-06-10 ‒ 2022-06-16 | Andre Sailer        | [report](https://codimd.web.cern.ch/Pf4qkdMdQMOg_KAmQDmxQw?view#Shift-from-June-10-to-June-16)             |
| 2022-06-03 ‒ 2022-06-09 | Gerardo Ganis  |  [report](https://codimd.web.cern.ch/7BaERfnbRyKydEFl1Bhz6g)            |
| 2022-05-27 ‒ 2022-06-02 | Harald Hansen       | [report](https://codimd.web.cern.ch/mLmiWmipT-uBHgVoxhs7sQ?view)             |
| 2022-05-20 ‒ 2022-05-26 | Dmitri Konstantinov |              |
| 2022-05-13 ‒ 2022-05-19 | Ilias Goulas        |              | 
| 2022-05-06 ‒ 2022-05-12 | Andre Sailer        |              |
| 2022-04-29 ‒ 2022-05-05 | Gerardo Ganis       | [report](https://codimd.web.cern.ch/2RqpV90USYe_GIPJDVZRkA)             |
| 2022-04-22 ‒ 2022-04-28 | Harald Hansen       |  [report](https://codimd.web.cern.ch/7Zkb_IEMSpKfr8ssH65CQw?view)            |
| 2022-04-15 ‒ 2022-04-21 | Dmitri Konstantinov |              |
| 2022-04-08 ‒ 2022-04-14 | Ilias Goulas        |  [report](https://codimd.web.cern.ch/s/Hl-kECXF5)             | 
| 2022-04-01 ‒ 2022-04-07 | Andre Sailer        |  [report](https://codimd.web.cern.ch/Pf4qkdMdQMOg_KAmQDmxQw?view#Shift-from-April-1-to-April-7)            |
| 2022-03-25 ‒ 2022-03-31 | Gerardo Ganis       |  [report](https://codimd.web.cern.ch/H-TfvPZJRB-NIV5qx1UDrg)             |
| 2022-03-18 ‒ 2022-03-24 | Harald Hansen |[report](https://codimd.web.cern.ch/s/jqjvtMVGG#)      |              |
| 2022-03-11 ‒ 2022-03-17 | Dmitri Konstantinov |              |
| 2022-03-04 ‒ 2022-03-10 | Ilias Goulas        | [report](https://codimd.web.cern.ch/s/E9wfQmD-j)             | 
| 2022-02-25 ‒ 2022-03-03 | Andre Sailer        | [report](https://codimd.web.cern.ch/Pf4qkdMdQMOg_KAmQDmxQw?view#Shift-from-February-25-to-March-3)             |
| 2022-02-18 ‒ 2022-02-24 | Harald Hansen       | [report](https://codimd.web.cern.ch/s/pzR8fYqJv#)    |
| 2022-02-11 ‒ 2022-02-17 | Gerardo Ganis | [report](https://codimd.web.cern.ch/et8MX9ojSV2RG8hpfF2JPw) |
| 2022-02-04 ‒ 2022-02-10 | Dmitri Konstantinov |              |
| 2022-01-28 ‒ 2022-02-03 | Ilias Goulas        |              | 
| 2022-01-21 ‒ 2022-01-27 | Andre Sailer        |              |
| 2022-01-14 ‒ 2022-01-20 | Gerardo Ganis | [report](https://codimd.web.cern.ch/xMuRgA-mQ5KMZ95qC69a9A) |
| 2022-01-05 ‒ 2022-01-13 | Harald Hansen       |   [report](https://codimd.web.cern.ch/s/edcL2g1vt)           |


## 2021

| WEEK                    | PERSON              |    REPORT    |
|:------------------------|:--------------------|--------------|
| 2021-12-17 ‒ 2021-12-22 | Dmitri Konstantinov             |  |
| 2021-12-10 ‒ 2021-12-16 | Ilias Goulas    | [report](https://codimd.web.cern.ch/s/mhOFE-b6U) |
| 2021-12-03 ‒ 2021-12-09 | Andre Sailer          | [report](https://codimd.web.cern.ch/Il_vsln0Smavx4RfTWLVWw?view#Shift-from-December-3-to-December-9)  |
| 2021-11-26 ‒ 2021-12-02 | Harald Hansen             |[report](https://codimd.web.cern.ch/O9BpNrXFS1GwaRHRwFxljA)  |
| 2021-11-19 ‒ 2021-11-25 | Gerardo Ganis             | [report](https://codimd.web.cern.ch/SBa_ar0FRVCbu-k_sgEggQ) |
| 2021-11-12 ‒ 2021-11-18 | Andre Sailer             | [report](https://codimd.web.cern.ch/Il_vsln0Smavx4RfTWLVWw?view#Shift-from-November-12-to-November-18) |
| 2021-11-05 ‒ 2021-11-11 | Dmitri Konstantinov    |         |
| 2021-10-29 ‒ 2021-11-04 | Ilias Goulas             | [report](https://codimd.web.cern.ch/s/uMJRihH6B) |
| 2021-10-22 ‒ 2021-10-28 | Gerardo Ganis             | [report](https://codimd.web.cern.ch/SkkKjh_EQNevSN5Fx5LquA) |
| 2021-10-15 ‒ 2021-10-21 | Harald Hansen             |  |
| 2021-10-08 ‒ 2021-10-14 | Dmitri Konstantinov             |  |
| 2021-10-01 ‒ 2021-10-07 | Andre Sailer    |  [report](https://codimd.web.cern.ch/Il_vsln0Smavx4RfTWLVWw?both#Shift-from-October-1-to-October-7)       |
| 2021-09-24 ‒ 2021-09-30 | Ilias Goulas             |  [report](https://codimd.web.cern.ch/s/zYJRgbZKn) |
| 2021-09-17 ‒ 2021-09-23 | Gerardo Ganis             |  [report](https://codimd.web.cern.ch/wMDdaSqwT46MnxgYIJiHxw)|
| 2021-09-10 ‒ 2021-09-16 | Harald Hansen             | [Report](https://codimd.web.cern.ch/Il_vsln0Smavx4RfTWLVWw#Shift-from-September-10-to-September-16) |
| 2021-09-03 ‒ 2021-09-09 | Ilias Goulas             | [Report](https://codimd.web.cern.ch/s/e928Yxtsy) |
| 2021-08-27 ‒ 2021-09-02 | Dmitri Konstantinov    |         |
| 2021-08-20 ‒ 2021-08-26 | Andre Sailer             |  |
| 2021-08-13 ‒ 2021-08-19 | Dmitri Konstantinov     |  |
| 2021-08-06 ‒ 2021-08-12 | Andre Sailer            |  |
| 2021-07-30 ‒ 2021-08-05 | Gerardo Ganis   | [Report](https://codimd.web.cern.ch/1tW2AVuXTlyERpuTBkCFSQ)  |
| 2021-07-23 ‒ 2021-07-29 | Gerardo Ganis      |   [Report](https://codimd.web.cern.ch/wRfSJU5EQh-E4hjvxAA1FA)       |
| 2021-07-16 ‒ 2021-07-22 | Ilias Goulas             | [Report](https://codimd.web.cern.ch/s/1gRaIjXfp)  |
| 2021-07-09 ‒ 2021-07-15 | Ari Kraut                | [Report](https://codimd.web.cern.ch/s/hLAOoQkI4)  |
| 2021-07-02 ‒ 2021-07-08 | Dmitri Konstantinov            |  |
| 2021-06-25 ‒ 2021-07-01 | Andre Sailer             | [Report](https://codimd.web.cern.ch/Il_vsln0Smavx4RfTWLVWw#Shift-June-25-to-July-1) |
| 2021-06-18 ‒ 2021-06-24 | Dmitri Konstantinov      |         |
| 2021-06-11 ‒ 2021-06-17 | Ilias Goulas             | [Report](https://codimd.web.cern.ch/s/zsgVEJW1Z) |
| 2021-06-04 ‒ 2021-06-10 | Ari Kraut                | [Report](https://codimd.web.cern.ch/s/ijgMixjqe) |
| 2021-05-28 ‒ 2021-06-03 | Gerardo Ganis            | [Report](https://codimd.web.cern.ch/s/UQO2ivAi6) |
| 2021-05-21 ‒ 2021-05-27 | Andre Sailer             | [Report](https://codimd.web.cern.ch/Il_vsln0Smavx4RfTWLVWw?view#Shift-May-21-to-May-27) |
| 2021-05-14 ‒ 2021-05-20 | Dmitri Konstantinov |                                                  |
| 2021-05-07 ‒ 2021-05-13 | Ilias Goulas        |  [Report](https://codimd.web.cern.ch/s/qTmfITPYa)|
| 2021-04-30 ‒ 2021-05-06 | Ari Kraut           |  [Report](https://codimd.web.cern.ch/s/Y54RKocJW)|
| 2021-04-23 ‒ 2021-04-29 | Ivan Razumov        |                                                  |
| 2021-04-16 ‒ 2021-04-22 | Gerardo Ganis       |  [Report](https://codimd.web.cern.ch/s/PEFT85b_8)|
| 2021-04-09 ‒ 2021-04-15 | Andre Sailer        |  [Report](https://codimd.web.cern.ch/s/rkxGZOi8v#Shift-April-9-to-April-15)  |
| 2021-04-02 ‒ 2021-04-08 | Dmitri Konstantinov |         |
| 2021-03-26 ‒ 2021-04-01 | Ilias Goulas        |  [Report](https://codimd.web.cern.ch/s/L_CDwDFTT)            |
| 2021-03-19 ‒ 2021-03-25 | Ari Kraut           |  [Report](https://codimd.web.cern.ch/s/hQTE_z6PN)            |
| 2021-03-12 ‒ 2021-03-18 | Ivan Razumov        |  [Report](https://codimd.web.cern.ch/s/bedc8WZPl)            |
| 2021-03-05 ‒ 2021-03-11 | Gerardo Ganis       |  [Report](https://codimd.web.cern.ch/s/XJBeB7VKS)            |
| 2021-02-26 ‒ 2021-03-04 | Dmitri Konstantinov |              |
| 2021-02-19 ‒ 2021-02-25 | Andre Sailer     | [Report](https://codimd.web.cern.ch/s/rkxGZOi8v#Shift-February-22-to-February-25)            |
| 2021-02-12 ‒ 2021-02-18 | Ilias Goulas             | [Report](https://codimd.web.cern.ch/DXBZELlfQJSa7MnQDKyjqQ) |
| 2021-02-06 ‒ 2021-02-11 | Ari Kraut                | [Report](https://codimd.web.cern.ch/s/opgLYkeyy) |
| 2021-01-29 ‒ 2021-02-05 | Ivan Razumov             | [Report](https://codimd.web.cern.ch/s/mPsx1r4Dq) |
| 2021-01-20 ‒ 2021-01-28 | Gerardo Ganis            | [Report](https://codimd.web.cern.ch/s/y6C8FUhLY) |
| 2021-01-13 ‒ 2021-01-19 | Andre Sailer             | [Report](https://codimd.web.cern.ch/s/rkxGZOi8v) |
| 2021-01-06 ‒ 2021-01-12 | Dmitri Konstantinov      |              |

## 2020

| WEEK                    | PERSON              |    REPORT    |
|:------------------------|:--------------------|--------------|
| 2020-12-16 ‒ 2021-01-05 | Ilias Goulas        |              |
| 2020-12-09 ‒ 2020-12-15 | Ari Kraut           | [Report](https://codimd.web.cern.ch/s/qHa8HVSBP) |
| 2020-12-02 ‒ 2020-12-08 | Ivan Razumov        | [Report](https://codimd.web.cern.ch/s/bedc8WZPl) |
| 2020-11-25 ‒ 2020-12-01 | Gerardo Ganis       | [Report](https://codimd.web.cern.ch/s/aIflxCHp7) |
| 2020-11-18 ‒ 2020-11-24 | Andre Sailer        | [Report](https://codimd.web.cern.ch/s/rkxGZOi8v) |
| 2020-11-11 ‒ 2020-11-17 | Dmitri Konstantinov |              |
| 2020-11-04 ‒ 2020-11-10 | Ari Kraut           | [Report](https://codimd.web.cern.ch/s/zS9mzhvdt) |
| 2020-10-28 ‒ 2020-11-03 | Ilias Goulas        | [Report](https://codimd.web.cern.ch/s/2oiQ_A0ac) |
| 2020-10-21 ‒ 2020-10-27 | Ivan Razumov        | [Report](https://codimd.web.cern.ch/s/Nx8WsVfO-) |
| 2020-10-14 ‒ 2020-10-20 | Gerardo Ganis       | [Report](https://codimd.web.cern.ch/s/B1NaSsBDD) |
| 2020-10-07 ‒ 2020-10-13 | Andre Sailer        | [Report](https://codimd.web.cern.ch/Il_vsln0Smavx4RfTWLVWw?view) |
| 2020-09-30 ‒ 2020-10-06 | Dmitri Konstantinov |              |
| 2020-09-23 ‒ 2020-09-29 | Ari Kraut           |              |
| 2020-09-16 ‒ 2020-09-22 | Ilias Goulas        |              |
| 2020-09-09 ‒ 2020-09-15 | Ivan Razumov        |              |
| 2020-09-02 ‒ 2020-09-08 | Dmitri Konstantinov |              |
| 2020-08-24 ‒ 2020-09-01 | Gerardo Ganis       |              |
| ~~2020-08-19 ‒ 2020-08-25~~ | ~~(Richard Bachmann)~~  |              |
| 2020-08-12 ‒ 2020-08-23 | Ivan Razumov        |              |
| 2020-08-05 ‒ 2020-08-11 | Ilias Goulas        |              |
| 2020-07-29 ‒ 2020-08-04 | Dmitri Konstantinov |              |
| 2020-07-22 ‒ 2020-07-28 | Gerardo Ganis       |              |
| 2020-07-15 ‒ 2020-07-21 | Ilias Goulas        |              |
| 2020-07-08 ‒ 2020-07-14 | Richard Bachmann    |              |
| 2020-07-01 ‒ 2020-07-07 | Ivan Razumov        |              |
| 2020-06-24 ‒ 2020-06-30 | Dmitri Konstantinov |              |
| 2020-06-17 ‒ 2020-06-23 | Gerardo Ganis       |              |
| 2020-06-10 ‒ 2020-06-16 | Richard Bachmann    |              |
| 2020-06-03 ‒ 2020-06-09 | Ilias Goulas        |              |
| 2020-05-27 ‒ 2020-06-02 | Ivan Razumov        |              |
| 2020-05-20 ‒ 2020-05-26 | Dmitri Konstantinov |              |
| 2020-05-13 ‒ 2020-05-19 | Gerardo Ganis       |              |
| 2020-05-06 ‒ 2020-05-12 | Richard Bachmann  |              |
| 2020-04-29 ‒ 2020-05-05 | Ilias Goulas        |              |
| 2020-04-22 ‒ 2020-04-28 | Ivan Razumov        |              |
| 2020-04-15 ‒ 2020-04-21 | Dmitri Konstantinov |              |
| 2020-04-08 ‒ 2020-04-14 | Gerardo Ganis   |              |
| 2020-04-01 ‒ 2020-04-07 | Richard Bachmann    |              |
| 2020-03-25 ‒ 2020-03-31 | Ilias Goulas        |              |
| 2020-03-18 ‒ 2020-03-24 | Ivan Razumov        |              |
| 2020-03-11 ‒ 2020-03-17 | Dmitri Konstantinov |              |
| 2020-03-04 ‒ 2020-03-10 | Gerardo Ganis       |              |
| 2020-02-26 ‒ 2020-03-03 | Richard Bachmann    |              |
| 2020-02-19 ‒ 2020-02-25 | Ilias Goulas        |              |
| 2020-02-12 ‒ 2020-02-18 | Ivan Razumov        |              |
| 2020-02-05 ‒ 2020-02-11 | Dmitri Konstantinov |              |
| 2020-01-29 ‒ 2020-02-04 | Gerardo Ganis       |              |
| 2020-01-22 ‒ 2020-01-28 | Richard Bachmann    |              |
| 2020-01-15 ‒ 2020-01-21 | Ilias Goulas        |              |
| 2020-01-08 ‒ 2020-01-14 | Ivan Razumov        |              |

## 2019

| WEEK                    | PERSON              |
|:------------------------|:--------------------|
| 2019-12-13 ‒ 2019-10-19 | Dmitri Konstantinov |
| 2019-12-06 ‒ 2019-12-12 | Gerardo Ganis       |
| 2019-11-29 ‒ 2019-12-05 | Richard Bachmann    |
| 2019-11-22 ‒ 2019-11-28 | Ilias Goulas        |
| 2019-11-15 ‒ 2019-11-21 | Ivan Razumov        |
| 2019-11-08 ‒ 2019-11-14 | Dmitri Konstantinov |
| 2019-10-31 ‒ 2019-11-07 | Gerardo Ganis       |
| 2019-10-24 ‒ 2019-10-30 | Ilias Goulas        |
