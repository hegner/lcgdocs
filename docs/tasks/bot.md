# Testing merge requests with LCG-Bot

* To test a new package (or a new version of an existing package) one must open 
a merge request and put `Bot test <package> in <toolchain> please!` as a comment 
(**not** as a description) of that merge request. 

* Bot can test many toolchains with predefined platforms

* One can specify more than one toolchain, separated with " " (space).

* `all` is a convenient alias for `dev3 dev4`

* One can test an aribtrary toolchain if the platform is also specified.

* The bot will post a reply confirming that the request was accepted.

* To repeat the first command use `Bot repeat please!`. Then the comment with the result will be updated

* If, for some reason, the bot does not reply, one should SSH to `lcgbot02.cern.ch` as `root`, `cd` to `/build` and execute `start-bot.sh`.

# Updating the bot
* If you update bot's source code or **bot account password**, one will need to rebuild it's Docker image

* Before building the image for the first time, or after changing the password, one needs to prepare keytab file:
```console
$ cern-get-keytab -u -k bot.keytab --login lcgbot
```

* Next, you will need to copy config.py.in and rename it as config.py. Then, fill in the placeholders with the appropriate information.

* Then, run the included `buildme.sh` script:
```console
$ docker login gitlab-registry.cern.ch
$ ./buildme.sh latest
```

* There is a gitlab webhook which notifies the bot of any new merge request or comment. The webhook has a shared secret with the bot. When using a new host the webhook as to be updated.

# For the curious
[GitLab repository](https://gitlab.cern.ch/sft/lcgbot) 
