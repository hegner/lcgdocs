# Cleaning a workspace

To manually reproduce the [`lcg-clean-builds`](https://epsft-jenkins.cern.ch/view/LCG%20Externals/job/lcg-clean-builds/) jenkins job, use the following commands once logged into the node to be cleaned:

```
/build/jenkins/workspace/lcg-clean-builds/LABEL/<node_name>
export WORKSPACE=`pwd`
lcgjenkins/cleanbuildmachines.py $(dirname $(dirname $(dirname $WORKSPACE)))
```

> **Note** If the target node runs `ubuntu`, the path may be slightly different (*without `jenkins` after `/build`*):
>
> ```
> /build/workspace/lcg-clean-builds/LABEL/<node_name>
> ```
