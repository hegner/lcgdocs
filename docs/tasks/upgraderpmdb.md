# Creating/recreating/updating a RPM database

The database of a RPM repository needs to be redone or updated each time the content of a RPM repository has been changed (RPMs added or removed). The operation below are typically run automatically by the scripts managed by Jenkins. In some cases manual operations may be required for which the descriptions and instructions are provided. It is typically worth, though not mandatory, running the commands from a powerful machine, e.g. `lcgapp-centos7-physical`; the machine must have r/w access to `/eos/project/l/lcg/www/lcgpackages/`.

## Location of the RPM database

The database is located in a directory `repodata` located at the top level of the repository. The RPM repositories for the LCG releases are located under the LCG EOS area, partitioned by RHEL reference OS version (6, 7, 8, 9, ...), architecture (x86_64, aarch64, ...), build type (opt, dbg) and type of RPM (core, metarpm). For example, for centos7, the repository of core RPMs, build type optimized, is located under
```
/eos/project/l/lcg/www/lcgpackages/lcg/repo/7/x86_64/Packages/
```
while the metaRPMs for the releases LCG_102 are located under
```
/eos/project/l/lcg/www/lcgpackages/lcg/repo/7/x86_64/LCG_102/
```

## Creation/update of a RPM database

Database are created by running the command `createrepo [--workers=n] path-to-repository`, e.g.
```
createrepo --workers=10 /eos/project/l/lcg/www/lcgpackages/lcg/repo/7/x86_64/Packages/
```
The directory 
```
/eos/project/l/lcg/www/lcgpackages/lcg/repo/7/x86_64/Packages/repodata/
```
is then created. The number of workers to be used depends on the number of RPMs to be processed. 
If the repository database does already exists, it is possible to update it with the command
```
createrepo --update --workers=10 /eos/project/l/lcg/www/lcgpackages/lcg/repo/7/x86_64/Packages/
```
