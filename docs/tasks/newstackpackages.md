# Adding new packages to the LCG stack

The LCG Stack requires constant renewal to stay up-to-date with new package releases and the demands of its users. This guide shows how new packages can be added to the stack. 

The stack is based on [the LCGCMake](https://gitlab.cern.ch/sft/lcgcmake) project on Gitlab.

## Select package to add
First of all you must find the package's project page. It can usually be found on [Github](https://github.com/), [Hepforge](https://hepforge.org/projects), or [Pypi](https://pypi.org/) in the case of python packages. Look for the documentation and install steps, and pay attention to listed additional dependencies. Resolve each dependency recursively using this guide before proceeding. Then find the download for the source files.  Make sure to select a release with a version number wherever possible. Additionally, you should select .tar.gz archives if available, as this is the format which is usually used by LCGCmake. 

1. Go to your work directory of choice.
!!! tip
    A quick guide for setting up a build environment on a 24 core physical machine is included below.

2. Clone the lcgcmake repository with `git clone ssh://git@gitlab.cern.ch:7999/sft/lcgcmake.git -b <branch name>` or simply pull in the most recent version if already cloned.
3. Create a new branch, containing the Jira ticket's code and the package's name in the branch name. 
4. Resolve dependencies. You can check whether or not these are already installed by querying [LCGInfo](http://lcginfo.cern.ch/).
5. Locate and download the correct source files. This can be done using your browser or wget, for example. 
    *  Download these to a temporary location, not the lcgcmake directory.

## Upload
If the downloaded files aren't inside a .tar.gz archive you may have to create a new tarball. In the following example we convert a .tar.xz to a .tar.gz file:
``` bash
tar xf <pkgname>.tar.xz
tar -cvzf <pkgname>.tar.gz <pkgname_out_dir>
```
Note that while LCGCmake can handle other formats than .tar.gz, it is easier if things are somewhat standardized. You can of course also change the recipe in the CMakefile, but doing so can break backwards compatibility. 

Also ensure that the tarball has a name which follows the format `<pkgname>-<pkgversion>.tar.gz`. 
Rarely the package in question won't have a valid release version, for example if the head of the `master` branch has to be used to implement a fix. In this case the short form of the git hash can be used instead of `pkgversion`. 

The package archive has to be uploaded to EOS before it can be used by the build process. There are three ways to accomplish this:

*  Jenkins job [lcg\_urls\_to\_eos](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_urls_to_eos/). Simply press `Build with Parameters` and provide the download link for the archive. Note that this method can't be used if the archive has to be converted.  
*  Manually copy the file using scp and sftnight: `scp filename.tar.gz sftnight@lxplus:/eos/project/l/lcg/www/lcgpackages/tarFiles/sources/`
*  Manually copy the file with xrdcp: `xrdcp -f filename.tar.gz root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources`

!!! important
    Generator sources go to /eos/project/l/lcg/www/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/

## Create build recipe
The build recipe specifies how LCGCMake should build the package in question, as well as which dependencies it requires. 

Within the lcgcmake directory there 5 subdirectories to which packages are usually added. These are:

*   ./externals/ (for generic external packages)
*   ./generators/ (for generators)
*   ./gridexternals/ 
*   ./projects/ 
*   ./pyexternals/ (for python packages)

Select the appropriate one, and open the CMakeList.txt file contained within. Go to the bottom of the file and add an entry for your package. The entry, which is specified using the helper function LCGPackage\_Add(), depends on the build instructions of the package itself. Look at other entries in the file for examples of how to implement certain steps in the helper function. A generic use of the function can look like:
```
LCGPackage_Add(
  <pkgname>
  URL ${GenURL}/<pkgname>-${<pkgname>_native_version}.tar.gz
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR>
  BUILD_COMMAND  ${MAKE}
  INSTALL_COMMAND ${MAKE} install
  BUILD_IN_SOURCE 1
)
```

If you are adding multiple entries, such as a package and its dependencies, ensure that they are ordered so that a dependency is declared before the dependee. 

The `pkgname` used here specifies how the package is registered within the system and displayed in LCGInfo. The build process carries with it the constraint that the use of dashes `-` is prohibited. If the pkgname contains dashes it will have to be renamed here by changing the first and third occurence of `pkgname` as listed in the example above. 

!!! warning
    Certain changes, such as only modifying the flags used in `CONFIGURE_COMMAND`, don't trigger a rebuild of the package on their own.
    If you are making such modifications to existing packages in order to fix something, add or increment the `REVISION` field.
    This will cause a guaranteed rebuild.

### Patches

Sometimes it is necessary to modify the source files for a given package. For this purpose we store patches in the
`patches` subfolder of the different subdirectories (`externals`, `pyexternals`, etc.). To create a patch we use
`emacs`, because that stores backup files (`*~`) parallel to the file we are going to modify. So

  1. Modify the necessary files with emacs, and `make <PACKAGE_NAME>` until the build is successful.
  2. Run, assuming one is in the `build` folder:
     ```
     for FILE in `find . -name *~`; do diff -U0 ${FILE} ${FILE%*~}; done > ../lcgcmake/<SUBDIRECTORY>/patches/<PACKAGE_NAME>-<VERSION>.patch
     ```
  3. Modify the paths in the patch file to be relative to the source code directory.
  4. Rebuild using your patch. First cleaning the existing installation, `make clean-<PACKAGE_NAME>` to remove the
     already modified files. Then rebuild the package from scratch `make <PACKAGE_NAME>`.
  5. `git add` the patch and `git commit`.

Of course this only works if you modify the file once, otherwise the backup file might be overwritten and some changes
would be lost. One is of course also free to use any other editor, but the principle is the same.


## Add package to toolchain
Once the build recipe is defined, the package in question has to be added to the right toolchain. These are specified in `./cmake/toolchain/`. To add a package to the whole stack, which is the most common case, modify `heptools-dev-base.cmake`. This file is split into several sections, such as python3, python2 and externals. 

1. Find the correct section within the file. 
2. The section's content should be ordered alphabetically. Find the correct line for the package to be inserted.
3. Insert the line `LCG_external_package(<pkgname <pkgversion>)` and match the existing formatting.  
4. Git commit and push your branch to the remote, then create a merge request to the master branch on Gitlab.

## Document 
Document your changes by adding package entries to `./documentation/packages.json`. These entries determine which information is displayed about the package on LCGInfo.

## Test
Test that your addition to the stack builds successfully by using the integrated Gitlab [bot](bot.md). For Python packages it is wise to test both dev3 and dev3python3, in case of dependency differences. 

It is also possible to test your branch with the Jenkins job *lcg_experimental*, which can be found on epsft-jenkins.cern.ch. Press `Build with Parameters`, select the desired platform combinations, insert your branch into `VERSION_MAIN`, consider whether or not the other parameters should be changed, and hit `Build`. Once completed the job's results can be viewed in CDash.

## Merge
If the tests succeed you can go ahead and merge into master. Ask your teammates for a go-ahead if you are in doubt about certain changes. 

## Tips and tricks

### Working from build node
The installation is done best on one of the build nodes (centos7 or slc6). It requires EOS access (which might be unavailable from local user) to "/eos/project/l/lcg/www/lcgpackages/tarFiles/sources". You can work from the build nodes by ssh-ing to the sftnight user:

``` bash
ssh sftnight@lcgapp-centos7-physical
```
From here make yourself a build directory. This has to be on /build/, as the home directory is too small for this task.
``` bash
mkdir -p /build/<username>/build
cd /build/<username>/
mkdir install
```
Then prepare your build environment.
``` bash
# Clone the repository
git clone ssh://git@gitlab.cern.ch:7999/sft/lcgcmake.git -b <branch name>
# Source correct versions of gcc, g++ for this build, e.g.
source lcgcmake/jenkins/jk-setup.sh Release gcc13
# Verify 
echo $CC
# Select stack and prepare build
cd build
cmake -DCMAKE_INSTALL_PREFIX=../install -DLCG_VERSION=dev3 -DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/releases ../lcgcmake
```
To speed up testing new packages, you can configure lcgcmake to pick up packages from CVMFS or the EOS latest files.

```
cmake -DCMAKE_INSTALL_PREFIX=../install \
      -DLCG_ADDITIONAL_REPOS=https://lcgpackages.web.cern.ch/tarFiles/latest \
      -DLCG_VERSION=dev3 \
      -DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/latest:/cvmfs/sft.cern.ch/lcg/releases \
      -DUSE_BINARIES=ON \
      ../lcgcmake
```

Now you can start the build with:
``` bash
make -j12 <pkgname>
```
If the build fails you can debug by looking at the output, then clean up and try again.
``` bash
# Cleanup
make clean-<pkgname-pkgversion> && make rebuild_cache
# Try again
make -j12 <package-version>
``` 

You can make a *view*, to also test if the package might be running, from the `build` folder
``` bash
../lcgcmake/cmake/scripts/create_lcg_view.py -B --delete --loglevel DEBUG -p x86_64-centos7-gcc11-opt -l $PWD/../install/dev3 $PWD/../views
```
Adapt the LCG_Version, i.e., `dev3` as necessary.

#### Working on a macOS machine

On our macOS machines, to make cmake find the correct compiler, you need to explicitly pass the compilers to cmake.
E.g.:

```bash
cmake -D LCG_ADDITIONAL_REPOS=https://lcgpackages.web.cern.ch/tarFiles/latest -DCMAKE_INSTALL_PREFIX=../install -DLCG_VERSION=dev4 -DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/latest:/cvmfs/sft.cern.ch/lcg/releases -DUSE_BINARIES=ON -DCMAKE_C_COMPILER=/usr/bin/clang -DCMAKE_CXX_COMPILER=/usr/bin/clang++ ../lcgcmake
```


### Ordering of the build recipe directories
The recipies in the different CMakeList.txt files are interpreted in the following order:

1. contrib
2. externals
3. gridexternals
4. projects
5. pyexternals
6. generators
7. frameworks

If cross-file dependencies which don't align with this ordering arise, it is recommended to place the dependencies into the same CMakeList as their parent. 

