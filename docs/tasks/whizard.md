### Context

Whizard package is non-relocatable, and can't be installed using the regular process. As all non-reclocatable packages, whizard needs
to be installed directly on the final location. For global installations, such those on cvmfs, this is often problem because the destination is often writable
only in special conditions and typically not during build time.
A possible solution in this case is to build the package on a machine with a writable layer overlayed on the read-only one.
This can be done with a container or with overlay technology, for example overlay-fs.

### Creating a writable layer

1. Creating a writable layer with docker

We need two terminal windows in this case:
    1. Start the docker container in privileged mode. You need this to bind-mount an install directory onto cvmfs:
```
$ docker run --name=whizard -it --rm -v /cvmfs:/cvmfs:shared -v /afs:/afs -v /ec:/ec -e SHELL=$SHELL -e RELEASE=<release> --cap-add=SYS_ADMIN gitlab-registry.cern.ch/sft/docker/centos7
```
Here `<release>` is the release tag without `LCG_`, for example `97` or `97a_FCC_1` .
    2. From a new shell on the same host, attach to the docker container as root:
```
$ docker exec -it -u 0 whizard /bin/bash
# mkdir -p /build/cvmfs && chown -R sftnight /build/cvmfs
# mount --bind /build/cvmfs /cvmfs/sft.cern.ch/lcg/releases/LCG_$RELEASE
```
      You can now go back to the initial shell and move to the build section below.

2. Creating a writable layer with overlay-fs

In this case, a useful tool is fuse-overlayfs, available on most of the modern linux distributions (the example below is with Ubuntu 20.04). 
In order to avoid problems with permissions, it is better alwasy to work with the same user owning the read-only file system. For cvmfs this
is typically cvmfs:cvmfs . You can check this by stating any of the files on the read-only file system.

If the user and group do no exist, create them with adduser and addgroup:
```
$ sudo addgroup cvmfs
$ sudo adduser --group cvmfs --shell /bin/bash cvmfs
```
You need to remount read-only cvmfs to a diffrent mount point; this is required to have the writable layer at the usual mount point.
If you are using autofs for cvmfs, you neet to stop and disable before
```
$ sudo service autofs stop  # systemd: sudo systemctl stop autofs
$ sudo chkconfig autofs off  # systemd: sudo systemctl disable autofs
```
otherwise just unmount the repository required:
```
$ sudo umount /cvmfs/sft.cern.ch
```
We need to mount it to another place, e.g. /mnt/cvmfs/sft.cern.ch
```
$ sudo mkdir -p /mnt/cvmfs/sft.cern.ch
$ sudo mount -t cvmfs sft.cern.ch /mnt/cvmfs/sft.cern.ch
```
Next is to create the writable layer. First we need to switch user:
```
$ sudo su - cvmfs
```
First we need to create some working dir:
```
$ mkdir /tmp/workdir /tmp/upperdir
```
use internally by overlay-fs; they should be on the same device.
Second we use fuse-overlay-fs to create the special mount point
```
fuse-overlayfs -o lowerdir=/mnt/cvmfs/sft.cern.ch -o worddir=/tmp/workdir -o upperdir=/tmp/upperdir /cvmfs/sft.cern.ch
```
The lower layer is the reference read-only layer, the mount point it the writable layer on top of it. Listing the mount point just
after creation should show the same content as the read-only lower layer. However, if evrything has worked, the mount point is writable.
You can check that trying to create a dumy file in it
```
$ cat > /cvmfs/sft.cern.ch/test.txt
A test
<Ctrl D>
$ rm -fr /cvmfs/sft.cern.ch/test.txt
```
If the above wroks without errors you are done.

### Performing the build

The build is done as usual in any writable build area; teh example is for centos7, gcc8:
```
$ source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/x86_64-centos7/setup.sh
$ export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/latest/Linux-x86_64/bin:$PATH
$ git clone https://gitlab.cern.ch/sft/lcgcmake.git
$ cd lcgcmake; git checkout LCG_${RELEASE}
# (Make sure that is defined in the relevant toolchain cmake/toolchain/heptools-<release>.cmake
# If necessary, add the line
#     LCG_external_package(whizard 2.8.2 ${MCGENPATH}/whizard)
# in there)
$ export OCAMLLIB=/cvmfs/sft.cern.ch/lcg/releases/LCG_${RELEASE}/ocaml/4.10.0/x86_64-centos7-gcc8-opt/lib/ocaml
$ cd ..; mkdir build; cd build
$ cmake -DCMAKE_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/releases/LCG_${RELEASE} -DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/releases -DLCG_VERSION=${RELEASE} ../lcgcmake
$ make -j8 whizard
# ... this will take some time...
```

### Package installation:
```
$ scp LCG_${RELEASE}_x86_64-centos7-gcc8-opt.txt sftnight@lxplus.cern.ch:/eos/project/l/lcg/www/lcgpackages/tarFiles/
$ cd /cvmfs/sft.cern.ch/lcg/releases/LCG_${RELEASE}/MCGenerators/whizard/<whizard_version>
$ tar -czf whizard-x86_64-centos7-gcc8-opt.tar.gz x86_64-centos7-gcc8-opt
$ scp whizard-x86_64-centos7-gcc8-opt.tar.gz sftnight@lxplus.cern.ch:/eos/project/l/lcg/www/lcgpackages/tarFiles/whizard-x86_64-centos7-gcc8-opt.tar.gz
```

#### Docker only: unmount bind-mounted directory and exist docker
If you are working with docker you need to unmount the build directory from the root shell
```
# umount /build/cvmfs
```
Exit docker instance.

### Installation on CVMFS

Connect to CVMFS manager machine and switch to cvmfs manager user

```
$ cvmfs_server transaction sft.cern.ch
$ cd /cvmfs/sft.cern.ch/lcg/releases/LCG_<version>/MCGenerators/
$ mkdir -p whizard/<whizard_version>; cd whizard/<whizard_version>
$ curl http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/whizard-x86_64-centos7-gcc8-opt.tar.gz | tar -xzf -
$ cd /cvmfs/sft.cern.ch/lcg/releases/LCG_<version>/
$ curl http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/LCG_$RELEASE_x86_64-centos7-gcc8-opt.txt | grep whizard
# Add the information about whizard to LCG_generators_x86_64-centos7-gcc8-opt.txt, in the correct format, i.e. something like this:
# whizard; f369b; 2.8.2; ./MCGenerators/whizard/2.8.2/x86_64-centos7-gcc8-opt; ocaml-a7d79,lhapdf-26856,looptools-8b7e0,hoppet-6f7d3,HepMC-d5a39,fastjet-c962b,gosam-70c04,gosam_contrib-779ba,FORM-99a41,qgraf-23be6,libtool-9ad34,LCIO-d6ab0,openloops-ccc7f
# (it requires some editing)
# Recreate the view using the scripts in the lcgcmake and lcgjenkins (need to pull these repsotories, if required)
$ lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases -p x86_64-centos7-gcc8-opt -r $RELEASE -d -B /cvmfs/sft.cern.ch/lcg/views/LCG_$RELEASE/x86_64-centos7-gcc8-opt
$ cd; cvmfs_server publish sft.cern.ch
```

### Cleanup

Remove the tarball (from any machine with eos, as sftnight):
```
$ rm /eos/project/l/lcg/www/lcgpackages/tarFiles/whizard-x86_64-centos7-gcc8-opt.tar.gz
```