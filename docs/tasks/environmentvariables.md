# Adding and modifying environment variables
There are three important mechanisms in LCGCMake that define which environment variables are made available. These are:

## Build recipe ENVIRONMENT 
Setting an ENVIRONMENT variable for a project makes it and its corresponding value available to that project during build time. Most often you will set some variable to `${<dependency>_home}`, in order to point the package to some dependency's home directory, or a subpath of the package's own install path by using `INSTALL_DIR`. This could for example look like: 

``` bash hl_lines="4"
#---go--------------------------------------------------------------------------
LCGPackage_Add(
  go
  URL ${GenURL}/go${go_native_version}.tar.gz
  ENVIRONMENT GOROOT_BOOTSTRAP=/cvmfs/sft.cern.ch/lcg/contrib/go/latest/${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND ${CMAKE_COMMAND} -E chdir <SOURCE_DIR>/src bash ./make.bash
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
)
```

## Template in cmake/environment
Template variables set in [lcgenv](https://gitlab.cern.ch/GENSER/lcgenv/)'s `env/` directory are used to create the `.env` file for the project corresponding to that file name. These usually contain straight forward exports, or sources from other files:

``` bash
export M4=${M4_HOME}/bin/m4
```

``` bash
test -s $ROOT_HOME/bin/thisroot.sh && source $ROOT_HOME/bin/thisroot.sh
```

!!! tip
    You may also find some of these variables defined in `cmake/environment/<project>.template`, which is a legacy feature. New ones should be added to the lcgenv repository.

## Definition in cmake/scripts/create\_lcg\_viev\_setup
The `create_lcg_view_setup_*` files specify environment variables which will be globally available in a view. In other words, these are made available when one runs `source setup.sh` for a view. There are two files: `create_lcg_view_setup_sh.in` and `create_lcg_view_setup_csh.in`, both of which have to me modified when changes are made. Since these are reused for all views, it is important to wrap definitions in one or several test to make sure that relevant locations/files exist in that view.

!!! tip
    Symlinks to most binaries in the view are created in the view's /bin/ directory, which can be accessed using `$thisdir/bin`. **This can be used to include references to relative directories.** Use `readlink` to get the symlink's full path, and `dirname` to reference any of the parent directories. 

``` bash
#---then PYTHON
if [ -x $thisdir/bin/python ]; then
    PYTHONHOME=$(dirname $(dirname $(readlink $thisdir/bin/python))); export PYTHONHOME
elif [ -x $thisdir/bin/python3 ]; then
    PYTHONHOME=$(dirname $(dirname $(readlink $thisdir/bin/python3))); export PYTHONHOME
fi
```
