# Adding a new OS label

The addition of a new OS might require the introduction of a new binary tag. There are a few places that needs to be checked or modified:

*  `cmake/modules/heptools-common.cmake`: make sure that the function `lcg_get_host_os` returns the expected value;
*  `jenkins/getPlatform.py`: the new OS needs to added as possible case;
*  `jenkins/jk-setup.sh`: make sure the new case is included
*  `jenkins/Jenkinsfile-{experimental,nightly,release}`: in `InDocker`, the new OS should not fail the test.
*  `jenkins/lcgcmake-macros.cmake`: in function `GET_CONFIGURATION_TAG` make sure that the correct OS is set.
*  `cmake/toolchain/systemtools.cmake`: add the packages that should be taken from the system
