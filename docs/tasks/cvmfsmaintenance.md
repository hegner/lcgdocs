# A collection of CVMFS maintenance jobs

1. [Creating a CVMFS dirtab](#creating-a-cvmfs-dirtab) 
2. [Create CVMFS catalogs for view in new Releases](#create-cvmfs-catalogs-for-view-in-new-releases)
3. [Delete old nightly views](#delete-old-nightly-views)

## Creating a CVMFS dirtab

In the root directory of each CVMFS repository, there's a optional file called `.cvmfsdirtab`., e.g. `/cvmfs/sft.cern.ch/.cvmfsdirtab`.
It contains a list of places where catalogs or sub-catalogs should be created.
During a publication, CVMFS checks that file to see whether a catalog should be created for the currently published directories and files.
It is created automatically during publication if this is the case.  

For the repository `sft.cern.ch` we tried to keep a very fine-granular structure in order to create small catalogs that in sum don't take too much space on the client's side.
As you can see in the listing below we switched from naming individual packages or directories to regular expressions/wildcards.
This globbing caused each publication to become super-slow or even abort due to internal failures.
This should not happen and will eventually be fixed on the CVMFS side.
Due to that incident we renamed the file which means it is no longer used during publications to `sft.cern.ch`.
Existing catalogs are still used but new ones are no longer created automatically
The repository `sft-nightlies.cern.ch` is not affected by these changes: Its `.cvmfsdirtab` is still in place and functional.  

A temporary solution to this problem is described in the next section [Create CVMFS catalogs for view in new Releases](#create-cvmfs-catalogs-for-view-in-new-releases). Ideally we would create a `.cvmfscatalog` automatically during view creation but that step helps to mitigate the imminent issues like too big catalgs at the view level.

```ini
# Ensure that the root catalogue under '/' doesn't grow too big
/lcg/*
! /lcg/nightlies/*

# Create a subcatalogue for each release, each package name and each package version
# Example: Matches all of the following paths and creates a catalogue in each location
# - /lcg/releases/hadoop
# - /lcg/releases/hadoop/2.7.5.1-1f419
# - /lcg/releases/hadoop/2.7.5.1-1f419/x86_64-centos7-gcc8-opt
/lcg/releases/*
/lcg/releases/*/*
/lcg/releases/*/*/*

# Create a subcatalogue for each Grid/MCGenerator package, each version and each platform
/lcg/releases/Grid/*/*/*
/lcg/releases/MCGenerators/*/*/*

# Create a subcatalogue for each contrib package, version and platform
/lcg/contrib/*
/lcg/contrib/*/*
/lcg/contrib/*/*/*
/lcg/contrib/gentoo/linux/usr/*

# Create a subcatalogue for each view and platform
/lcg/views/*
/lcg/views/*/*
! /lcg/views/dev*/*

# Create a subcatalogue for each external package, package name, version and platform
/lcg/external/*
/lcg/external/*/*
/lcg/external/*/*/*

/lcg/external/MCGenerator*/*/*/*

# Create a subcatalogue for each app/release package, package name, version and platform
/lcg/app/releases/*
/lcg/app/releases/*/*
/lcg/app/releases/*/*/*

# Create a subcatalogue for each dev package, package name, version and platform
/lcg/dev/*
/lcg/dev/*/*
/lcg/dev/*/*/*

# AoB
/lcg/hepsoft/*
/lcg/etc/hadoop-confext/*
```

## Create CVMFS catalogs for view in new Releases

To avoid too many files in parent catalogs such as the catalog for `/cvmfs/sft.cern.ch/lcg/views`, we create empty files called `.cvmfscatalog` in each directory for which a sub catalog should be created.
This leads to smaller catalogs and thus less space to be consumed in the client's cache.
This procedure is only necessary as long as we don't fix the `.cvmfsdirtab` in `sft.cern.ch`.

With the following script run as user `cvsft` on the release manager `cvmfs-sft.cern.ch` you can create these `.cvmfscatalog` files for each platform in all releases starting with `LCG_96*`.
You can also select another LCG Release by setting the environment variable `LCG_VERSION` to the correct number:

```bash
LCG_VERSION=95 source <( curl --silent https://gitlab.cern.ch/snippets/903/raw?inline=false )
```

<script src="https://gitlab.cern.ch/snippets/903.js">

</script>

## Delete old nightly views

We only overwrite the views for nightly builds but never remove old nightlies that are no longer updated.
They are still symlinked with the `latest` tag on `sft.cern.ch` which might be misleading.
That's why we clean up old nightlies from time to time.


