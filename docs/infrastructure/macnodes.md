# Software needed to be installed in MacOS nodes

For the MacOS nodes used for building and releasing we require a number of packages being installed locally in the node.

## Command Line Tools
Compiler and basic development tools (without the need to install Xcode). 

- Launch the Terminal, found in /Applications/Utilities/
- Type the following command:
  ```
  xcode-select --install
  ```

## Fortran compiler (not in the command line tools)
- Download a package installer from: https://github.com/fxcoudert/gfortran-for-macOS/releases
- Follow instructions (if you have console access)
- From command line:
  ```
  curl -O -L https://github.com/fxcoudert/gfortran-for-macOS/releases/download/12.2-ventura/gfortran-ARM-12.2-Ventura.dmg
  hdiutil attach gfortran-ARM-12.2-Ventura.dmg
  sudo installer -pkg /Volumes/gfortran-ARM-12.2-Ventura/gfortran.pkg -target /
  mkdir -p /usr/local/lib
  cd /usr/local/lib
  sudo ln -s ../gfortran/lib/libgfortran.5.dylib
  sudo ln -s ../gfortran/lib/libquadmath.0.dylib
  ```

## CMake
- Download the package installer from: https://cmake.org/download/

## XRootD (needed to copy artifacts to EOS)

- Build the package from sources with the following commands
  ```
  export XROOTD_VERSION=5.7.2
  export ARCH=$(uname -m)
  export CLANG_VERSION=$(clang --version | grep version | sed "s/.*version //" | sed "s/\..*//")
  curl -L https://github.com/openssl/openssl/releases/download/openssl-3.4.0/openssl-3.4.0.tar.gz -O
  LC_ALL=C tar xzf openssl-3.4.0.tar.gz
  cd openssl-3.4.0/
  LC_ALL=C ./Configure --prefix=/Users/sftnight/xrootd/openssl/3.4.0/
  make -j 10
  make -j 10 install
  cd ../
  curl -O https://xrootd.web.cern.ch/download/v${XROOTD_VERSION}/xrootd-${XROOTD_VERSION}.tar.gz
  LC_ALL=C tar xzf xrootd-${XROOTD_VERSION}.tar.gz
  cd xrootd-${XROOTD_VERSION}
  mkdir build
  cd build
  cmake -D OPENSSL_ROOT_DIR=/Users/sftnight/xrootd/openssl/3.4.0/ -D CMAKE_INSTALL_PREFIX=/Users/sftnight/xrootd/${XROOTD_VERSION}/${ARCH}-clang${CLANG_VERSION}/ ..
  make install -j 10
  cd /Users/sftnight/xrootd/
  ln -sf ${XROOTD_VERSION}/${ARCH}-clang${CLANG_VERSION} latest
  ```

## XQuartz
- Download .dmg file from https://www.xquartz.org, mount the volume and install `XQuartz.pkg`
  ```
  curl -O -L https://github.com/XQuartz/XQuartz/releases/download/XQuartz-2.8.1/XQuartz-2.8.1.dmg
  hdiutil attach XQuartz-2.8.1.dmg   
  sudo installer -pkg /Volumes/XQuartz-2.8.1/XQuartz.pkg -target /
  ``` 

## CernVM
- Follow the instructions in https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html#mac-os-x


## Other Security Settings
### Kerberos keytab to copy files with XrootD to EOS
- Copy the keytab to `/Users/sftnight/conf/sftnight.keytab`

### Mount/unmount CVMFS without password
- Add the following lines in the sudoers file with the command: `sudo visudo`
```
# sftnight can mount and unmount CVMFS volumes
sftnight  ALL= NOPASSWD: /sbin/umount , /sbin/mount
```

