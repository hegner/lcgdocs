# Monitoring Machines

We are currently monitoring all machines running the Puppet LCGAPP configuration.

Data is collected using a push method: a cron job runs every minute, executing all relevant scripts and sending the information to the [Graphite](https://filer-carbon.cern.ch/) server.  
The collected data is visualized using our [Grafana](https://monit-grafana.cern.ch) instance.

To access the Grafana visualization for the LCG section, users must(?) be registered in the LCG organization within Grafana.

### Known Issues:
- **Node Selection in Grafana**: It is currently not possible to select nodes directly through the Grafana interface.  
- **Temporary Workaround**: Use the links to nodes provided in the [Nightly Build Reporter](https://lcgdocs.web.cern.ch/lcgdocs/report/).

## Build Nodes Monitor

This dashboard provides general machine information.

### Data collection
All the scripts are stored on the machine under `/var/stats/`. The `collect_stats.sh` is running every minute as a cron
job. The statistics and plugins are managed by [puppet](puppet.md).

Example of including plugins in hiera
```
hg_lcgapp::module::stat::plugins:
    - general
    - load
    - memory
    - storage build
    - ioload vdb
```

### Adding more data

The `collect_stats.sh` script, that is calling all the plugins, is exporting `sendstats` function that is sending the information to the graphite server.
```
sendstas    "$METRICPATH.<category>.<name>" <value> `$DATA_CMD`
#function   path in graphite                value   timestamp
```

To be able to install automatically new plugin in several machines, you have to add a new file (that is also a plugin name) in `code/files/stats/plugins` inside the lcgapp hostrgroup puppet configuration.

## Other possibilities
- Improvement of docker statistics - currently the data is collected but not everything is visualized
- [CVMFS server monitoring](https://indico.cern.ch/event/746547/contributions/3086277/attachments/1704994/2746996/Gathering_CVMFS_statistics1.pdf)
- Monitoring jenkins - number of tasks, how many in queue, machines available, awaiting time, task execution time


