# Openstack tasks

### Basic requirements

Various permissions need to be acquired in order to manage machines with the CERN Configuration Service infrastructure. Follow [these steps](http://configdocs.web.cern.ch/configdocs/tutorial/permissions.html#basic-requirements) to get all the necessary permissions to be able to get going. It includes permissions to use both: openstack and puppet.

### `aiadm` nodes

You need to SSH into `aiadm` to use the following commands.

### Create a new virtual machine

Common command to create our build nodes:

```
ai-bs  --foreman-hostgroup <hostgroup>           \
          --nova-image <image>                      \
          --nova-flavor <flavor>                    \
          --landb-responsible <responsible_group>   \
          <vm-name>
```

where:

- `--foreman-hostgroup`: define the foreman hostgroup, `lcgapp/<architecture>/<purpose>/<platform>/<specialisation>` in our case
- `--nova-image`: SO image tag (cc7, slc6, ...)
- `--nova-flavor`: Architectural configuration (CPUs, RAM, disk, ...)
- `--landb-responsible`: Responsible group
- `<vm-name>`: Virtual machine name, common convention `lcgapp-<OS>-<Arch>-<Number>`:
    * `lcgapp`: Name of the node dedicated to LCG purposes
    * `<OS>`: Contains name of the Operative System plus the major version
    * `<Arch>`: Either `x86-64` or `i386`
    * `<Number>`: Number to distinguish machines with the same `<SO>` and `<Arch>` configuration

`Forman hostgroup`
The idea behind above construction to reuse as much common configuration as possible. To achive this, we use the
hierarchical property of puppet (compiling _combined_ configuration from the root (lcgapp) to the most specific
configuration, e.g. platform)
Outlook of the hostgroup path:

- `lcgapp`: our hostgroup name
- `architecture`: specify processor specific configuration (i386, x86_64, [arm])
- `purpose`: define the purpose of the machine ( build, service, project)
- `platform`: define system specific configuration (cc7, slc6, ...)
- `specialisation`: define system specialisation (docker)

The project and service might omit the platform and define more sutable setup. 
E.g. lcgapp/x86_64/service/web/jenkins, lcgapp/x86_64/project/g4

Example for a new build node with SLC6 called `lcgapp-slc6-x86-64-1`:

```
ai-bs --foreman-hostgroup lcgapp/x86_64/build/slc6 
         --slc6 
         --nova-flavor "m2.xlarge" 
         --landb-responsible VOBOX-RESPONSIBLE-SFT 
         --nova-attach-new-volume vdb=320GB  
         --foreman-environment qa 
          lcgapp-slc6-x86-64-1
```

#### Create a new virtual machine (docker host)

The main difference is the `foreman-hostgroup`. This will provide a functioning machine, although it is recommended to
attach 2 external volumes (First will be build directory /mnt/build, the second will be docker directory /mnt/docker)

```
ai-bs --foreman-hostgroup lcgapp/x86_64/build/cc7/docker 
         --cc7 
         --nova-flavor "m2.xlarge" 
         --landb-responsible VOBOX-RESPONSIBLE-SFT 
         --nova-attach-new-volume vdb=320GB 
         --nova-attach-new-volume vdc=150GB
         --foreman-environment qa 
         lcgapp-centos7-x86-64-11
```

Our current docker hosts are configured to work with an external attached volume. Follow these steps to
attach one to the new virtual machine:

1. Enter to [https://openstack.cern.ch](https://openstack.cern.ch)
2. Switch to your project (PH LCGAA in our case)
3. Go to *Volumes* at the side menu
4. Click *+ CREATE VOLUME* button
5. Name the volume, current convention: `sft-disk-<SO>-<Number of the VM>`
    * Example: `sft-disk-cc7-39`
6. Add the description:
    * Example: *Disk for docker-host (lcgapp-centos7-x86-64-39)*
7. Leave *Volume Source* with the default value (*NO SOURCE, EMPTY VOLUME*)
8. Set *Type* to *Standard*
9. Usually, 320GB are used for docker hosts in *Size (GiB)*

![create_new_openstack_volume](/images/newvolume.png)

Now it is time to attach it to the new virtual machine:

1. Go to *Instances* at the side menu
2. Find the new virtual machine
3. Click on the arrow next to the button *CREATE SNAPSHOT*
4. Click on *Attach volume*
5. Select a volume
6. Click *Attach volume*

#### Using (an) existing volume(s)

It is possible to attach one or more existing volumes with the switch `--nova-attach-new-volume vdb=<Cinder ID>`, for example

```
ai-bs --foreman-hostgroup lcgapp/coverity
         --cc7
	 --nova-flavor "m2.3xlarge"
         --landb-responsible VOBOX-RESPONSIBLE-SFT
         --nova-attach-existing-volume vdb=68f4c471-912d-4e28-a64f-a9ab80e6822e
         --foreman-environment qa
          lcgapp-coverity
```

The Cinder ID can be obtained from the Openstack interface described above, by clicking on the existing volume:

![get_openstack_volume_id](/images/volumeID.png)


#### Recreate an existing virtual machine

This command rebuilds the machine

```
ai-rebuild-vm --cc7/--slc6 <vm_name>
```

Example:

```
ai-rebuild-vm --centos7 lcgapp-centos7-x86-64-36
```

# FAQ
## Why is there a separate volume for docker?
Sometimes docker has problem with closing the descriptor file. That causes problems with removing containers. The idea
behind external volume it to force unmounting the volume and attach it again, what should solve the problem.

## Whats the difference between cenots7 and centos7 docker platform? Can centos7 docker work as normal build machine?
The centos7 docker build exactly the same way as normal centos7 platform with addition of docker package and interface
for mounting additional volume for docker. The centos7 docker platform can be used as normal build machine. For now, we
use them exclusively for docker, because other tasks might take the docker machines, that actually do not need it and
block the docker tasks.




> **You should know that...**
> This action may not work for every virtual machine. See more info [here](http://configdocs.web.cern.ch/configdocs/nodes/rebuild.html)
