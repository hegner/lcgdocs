# Adding hosts to the jenkins infrastructure

### Docker hosts

1. Follow the steps in [Create a new virtual machine (docker host)](openstack.md) to create a new OpenStack virtual machine.
2. Don't forget to create the new volume and attach it.
3. Add a ssh key to access using `sftnight` credentials.
    * From any existing node, use `scp` to copy it over the same path in the new vm
4. Add it as a new Jenkins node:
    * Go to ["Manage Jenkins" / "Manage nodes and clouds"](https://lcgapp-services.cern.ch/spi-jenkins/computer/)
    * Click on *New node*
    * Insert the name of the new node in the field "Node name: lcgapp-cs8-x86-64-1" without extension ‘.cern.ch’
    * Copy it from an existing one, i.e. `lcgapp-cs8-x86-64-0`
    * Eventually modify the 'Description' field if necessary
    * Set up the correct bunch of labels
    * Don't forget to set the correct node name in the 'launch' field: 
        * eg: [/var/lib/jenkins/.ssh/launch slave.sh lcgapp-cs8-x86-64-1.cern.ch]
