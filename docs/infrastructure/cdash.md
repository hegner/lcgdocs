# CDash Service

The CDash service for the EP-SFT group runs on: [https://lcgapp-services.cern.ch/cdash3/projects](hhttps://lcgapp-services.cern.ch/cdash3/projects).
Its current version is **3.4** and it runs in a server managed by puppet, accessible from the terminal through `lcgapp-cdash-1.cern.ch`.

## Database

The current database is provided by the [DBoD Service](https://cern.ch/DBOnDemand) (Database On Demand):

- **Type:** MySQL
- **Host:** `dbod-cdash3-production.cern.ch`
- **Login:** `admin`
- **Port:** `5511`
- **Database:** `cdash`

## Installation

The code can be found at `https://gitlab.cern.ch/hegner/CDash/-/tree/cdash3` in the branch cdash3.

```
mkdir /opt/cdash
cd /opt/cdash
git clone 
https://gitlab.cern.ch/hegner/CDash/
cd CDash
cp .env_secure .env
```
Then add the DB password to the `.env.` file by setting `DB_PASSWORD`.


## Configuration

The configuration is entirely stored in the `.env.` file.


## Visibility to outside
The visibility to outside is managed via the lcgapp-services portal, at the moment via `lcgapp-services/cdash3`. To make this work, one has to configure lcgapp-services to forward `/cdash3` to `lcgapp-cdash-1.cern.ch`, while the cdash3 server `.env` file needs a setting of `APP_URL=https://lcgapp-services.cern.ch/cdash3`.


## Starting up the container.

Just invoke `podman-compose -f docker/docker-compose.yml -f docker/docker-compose.production.yml  --env-file .env up -d` within /opt/cdash/CDash


### Maintenance tasks
There are two possible maintenance tasks to deal with performance issues:

  1. pruning the database if its performance degrades. This can happen automatically, by setting `AUTOREMOVE_BUILDS=true` in the `.env` file. Every project can set its own preservation time in the web interface.
  2. pruning the old log files. This happens automatically in the new CDash version.
