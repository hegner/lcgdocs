# EP-SFT: Internal Documentation for SPI related activities

Welcome to the documentation of the SPI team. We are part of the SFT group in the EP department at CERN.
This documentation servers multiple purposes:

* Introduce our **users** to the *LCG Releases* and help them getting started
* Document *LCG CMake* **internals** for ourselves and future colleagues
* Describe the **operations** such as common tasks related to VMs, Puppet, Docker, CVMFS etc.

The currently available topics are:

### Introduction

* [Introduction to LCG Releases for end users](lcgreleases/introduction.md)

### LCG nightlies and operations

* [LCG operations](lcg/ops.md)
* [Special Packages](lcg/lcgcmake.md)
* [Jenkins jobs](lcg/jenkins.md)
* [Howto EOS](lcg/eos.md)
* [Adding new compilers](lcg/contrib.md)
* [Incremental builds](lcg/incrementalbuilds.md)
* [LCG PIP mirror](lcg/pipmirror.md)
* [LCG Spack builds](lcg/spack.md)
* [MacOS releases](lcg/macos-releases.md)

### LCG machine management

* [Openstack](infrastructure/openstack.md)
* [Puppet configuration](infrastructure/puppet.md)
* [Dockers in LCG](infrastructure/dockers.md)
* [Adding hosts to jenkins infrastructure](infrastructure/newhosts.md)
* [Adding CERN user accounts](infrastructure/newcernuser.md)
* [Adding new system packages to Docker and Puppet](tasks/newpackages.md)
* [Acquiring sftnight credentials](infrastructure/sftnight.md)
* [MacOS Nodes](infrastructure/macnodes.md)

### Common LCG tasks

* [Cleaning Build Nodes](tasks/cleanup.md)
* [Install a new `gcc` compiler](tasks/installgcc.md)
* [A collection of CVMFS maintenance jobs](tasks/cvmfsmaintenance.md)
* [The GitLab Bot](tasks/bot.md)

### SPI Shift

* [Assignments](shift/assignments.md)
* [Checklist](shift/checklist.md)
