# Supporting Incremental Nightly or Release builds 

## Enabling incremental builds with LCGCMake

The [LCGCMake](https://gitlab.cern.ch/sft/lcgcmake) code supports two different ways to support incremental builds that can be combined together. The current configuration for all the nightly builds (`dev3`, `dev4`) have enabled both mechanisms.

### The `LCG_INSTALL_PREFIX` configuration option
When configuring LCGCMake the option `LCG_INSTALL_PREFIX` indicates an area for binary installations. This option can be a concatenation (with ':' as separator) of a number of areas with a given search order. For example,
```
-DLCG_INSTALL_PREFIX=/cvmfs/sft.cern.ch/lcg/releases:/cvmfs/sft.cern.ch/lcg/latest
```
will be looking for installed packages in `.../lcg/releases` and `.../lcg/latest`. Symbolic links will be created in the local install area of the build node to the binary package installation (most of the time in CVMFS) if a full match of `<package>/<version>-<hash>/<platform>` exists in the LCG install prefix(es). 

### The `USE_BINARIES` configuration option
The option `USE_BINARIES` enables LCGCMake to look for binary tarfiles of packages in the main binary tarfiles repository (http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases). Additional repositories can be added with the option `LCG_ADDITIONAL_REPO`. A binary tarfile repository is a URL of a folder with tarfiles of the form `<name>-<version>_<hash>-<platform>.tgz` and a special file called `summary-<platform>.txt` used for speeding up lookups in the folder. If a tarfile exists in the repository, it will be downloaded, decompressed, expanded, and the `.postinstall` script executed in the local build node installation area  pointed by `${CMAKE_INSTALL_PREFIX}`. A directory in `${CMAKE_INSTALL_PREFIX}/${LCG_VERSION}` will created with links to the locally installed packages. Note that the effective local install area for the LCG stack is then `${CMAKE_INSTALL_PREFIX}/${LCG_VERSION}`.

These two options can be be used at the same time. The precedence is as follows:

- The `LCG_INSTALL_PREFIX` Will be checked first if an installation of a package already exists.
- If not, the the binary repository for tarfiles will be checked and eventually the tarfile downloaded and installed locally.
- Finally, the build will be done by downloading the package sources and executing the build recipe. 

## Populating a cache with binaries tarfiles and temporary installations
To speedup the LCG nightlies, fully qualified binaries are re-used currently for the LCG nightlies for subsequent builds. The process implemented in Jenkins is as follows:

* If the option `LCG_TARBALL_INSTALL` is enabled, the build will create fully qualified binary tarfiles in the `/tarfiles` folder in the build directory.
* The corresponding Jenkins job with the help of the script [jenkins/copytoLatest.sh](https://gitlab.cern.ch/sft/lcgcmake/-/blob/master/jenkins/copytoLatest.sh) will upload all the binary tarfiles to the EOS area `/eos/project/l/lcg/www/lcgpackages/tarFiles/latest` together with a copy of the LCG text file with the buildinfos corresponding to the `LCG_VERSION` and the generation of the summary file. The nightly jenkins pipeline will also copy tarfiles to `/eos/project/l/lcg/www/lcgpackages/tarFiles/nightlies/<STACK>/<WEEKDAY>` using the script [jenkins/copytoEOS.sh](https://gitlab.cern.ch/sft/lcgcmake/-/blob/master/jenkins/copytoEOS.sh)
* In order to keep the binary tarfile cache under control. The Jenkins job [lcg_purge_binary_repository](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_purge_binary_repository/) runs once a week to delete unused binary tarfiles older than 7 days. Unused tarfiles are identified by looking at the current buildinfo text files for all versions. This is done using the script [lcgcmake/cmake/scripts/purge_binary_repository.py](https://gitlab.cern.ch/sft/lcgcmake/-/blob/master/cmake/scripts/purge_binary_repository.py)
* The binary tarfiles in `/latest` are installed regularly (once a week or manually) to the CVMFS area ` /cvmfs/sft-nightlies.cern.ch/lcg/latest` using the Jenkins job [lcg_cvmfs_install_latest](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_cvmfs_install_latest). Old installations originated from already purged tarfiles will be deleted. This job is a multi-configuration job with a matrix of platforms to be installed in the `latest` installations.
* Command to update summary files for latest: `/eos/project/l/lcg/www/lcgpackages/tarFiles/latest/make-summaries <PLATFORM>` or the [lcg_summaries_creation](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_summaries_creation/) jenkins job.
* The eos nightlies folders are cleaned by the [lcg_clean_eos_nightlies](https://lcgapp-services.cern.ch/spi-jenkins/job/lcg_clean_eos_nightlies/) job on the day before.
