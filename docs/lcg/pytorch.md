# PyTorch
The torch package is built from sources. Since there are no source tarfiles available, this needs to obtained from the github repository. The tricky part is that PyTorch comes with a [long] number of third party packages as git submodules. There are the instructions for obtaining the complete source tarfile.

    git clone --recursive --branch  v<version>  https://github.com/pytorch/pytorch
    find pytorch -name .git -exec rm -rf {} \;
    tar -zcf torch-<version>.tar.gz pytorch

Note that the package is called 'torch', which is the name of the Python module.
