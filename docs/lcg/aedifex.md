# The aedifex build scripts

The [`aedifex`](https://gitlab.cern.ch/sft/aedifex) build scripts are a try to rewrite the [`lcgjenkins`](https://gitlab.cern.ch/sft/lcgjenkins) scripts to get a cleaner and more concise and consistent  build setup.


