# Two factor authentication

!!! quote
    From the 20th of April, AIADM (ssh://{aiadm, aivoadm, aiadm-homeless, aiadm-eos}.cern.ch) will be protected by 2FA. A time-limited single-factor parallel service (ssh://aiadm-single.cern.ch) will be made available on the same date. - [OTG0055962](https://cern.service-now.com/service-portal/view-outage.do?n=OTG0055962)

The use of 2fa at CERN is likely to expand to more systems in the future.

Valid 2nd factors are OTP generator apps (for your smartphone) and Yubikey. More information about 2fa at CERN can be found [here](https://security.web.cern.ch/recommendations/en/2FA.shtml), and a guide for registering a device is available [here](https://cern.service-now.com/service-portal/article.do?n=KB0006587).

## Binding a non-IT Yubikey 
In addition to the Yubikeys provided by IT it is possible to use a personal/externally acquired Yubikey for your CERN account. In order to work with SSH, it has to be registered in the IT group's database. To register it, do the following:

1. Download and install a tool for customizing the Yubikey. "Yubikey Personalization Tool" for example.
2. Yubikeys usually have multiple (2) slots that can be set to produce different secrets. Configure one of the slots to produce a OTP token, slot 1 (short press) is recommended. 
3. During step 2, write down the following: The device *serial number*, *public ID*, *private ID*, and *secret key*. **Store/send this data securely**, as it can be used to duplicate the key.
4. Encrypt a document with the data from step 3 using GPG, and send it in encrypted form to the computer security team together with your request to have the Yubikey registered. An example of how this can be done is provided below.

### Sending a GPG encrypted email to the computer security team

-----------------

**Prerequisites:**

* A gpg key `my_key`, with the public key uploaded to a keyserver where others can find it. A tutorial on generating this key can be found [here](https://www.digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages). gen-keys and send-keys is especially relevant for this step.
* A text file `yubikey_data.txt` which contains the secret data from step 3 above. 

-----------------

1.  Import the gpg key of the CERN Security team, found at the bottom right of [cern.ch/security](https://cern.ch/security).

    ```
    gpg --recv-keys 429D60460EBE8006B04CDF02954CE234B4C6ED84
    ```

    (Verify that these match, don't just copy-paste)

2.  Optionally, if you have a ticket open with one of the security team's members, you can import their key as well and verify that it is signed with the key above.

    ```
    gpg --list-sig <key_id>
    ```

3.  Encrypt the file using your private key and the security team's (or team member's) public key.

    ```
    gpg --sign --encrypt --armor --local-user <my_key> --recipient <security-key>
    ```

4.  Send the file `yubikey_data.txt.asc` to the security team with your request. 

5.  Once you have confirmation that it is registered you can [set up your Yubikey with WebAuthn](https://cern.service-now.com/service-portal/article.do?n=KB0006587).

6.  Register the Yubikey as a device for ssh sign-on [here](cern.ch/sshsetup).

!!! tip
    Note that at the time of writing, a TOTP authentication method is *required* in order to configure a WebAuthn with a Yubikey.
