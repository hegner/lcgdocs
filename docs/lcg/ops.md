# LCG operations

The whole infrastructure consist of several components that needs to be constantly adjusted depending on the needs of
clients and the needs of the changing software.

The main components that allow us to operate are:
- [LCGCMake](lcgcmake.md)
- [Jenkins](jenkins.md)
- [CVMFS](cvmfs.md)
- [EOS](eos.md)
- [Build machines](../infrastructure/openstack.md)
- [Docker](../infrastructure/dockers.md)

The LCG provides periodically a release and daily nightlies.

##  How it operates - connections between components

The most important part is provided as a set of tasks inside jenkins that cooperates with all the components in a well
defined order. The most common flow goes like this:

1. We trigger a build task ([more information](jenkins.md))
2. Jenkins setups the environment ([docker](../infrastructure/dockers.md) if needed, compiler)
3. Running [LCGCMake](lcgcmake.md)
  a. Create tarballs (part of lcgcmake step)
  b. Create RPMs (currently only during release)
4. Copy tarballs to Jenkins master node
5. Copy tarballs from Jenkins master node to [EOS](eos.md)
6. Install into cvmfs from tarballs inside EOS
7. Publish information to [lcginfo](http://lcginfo.cern.ch/)
8. [Run test on installed software](lcgtest.md)

For more advanced information about what exactly jenkins is doing, check [here](jenkins.md)

## Before building - new packages, upgrades and fixes

Because LCG is a software stack, it requires adding new packages, updating old packages and creating fixes to the
existing one.

### Installing new sources 
The installation is done best on one of the build nodes (centos7 or slc6). It requires EOS access (that might be not
available from local user) to "/eos/project/l/lcg/www/lcgpackages/tarFiles/sources". 

1. Goto a chosen work directory
2. Download the sources to this directory (e.g. wget)
3. Modify or repack if needed ( The sources need to be in .tar.gz )
4. Rename to <pkgname>-<pkgversion>.tar.gz
5. Copy to eos ( `xrdcp -f <pkgname>-<pkgvers>.tar.gz root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources` or cp)

#### Automate it!
The goal is to do as little work as possible. The best way would be to automate the whole process, i.e. listen to
publication channel of the package (e.g. github), if there is release, download sources, update version in lcgcmake,
push a Pull Request and test if it compiles. 

The current status is a single automatized package, i.e. NxCals.
```bash
cd lcgjenkins/autosource
./nxcals.sh 0.1.106 # as an argument give version
./install.sh # This will install all files from sources directory to EOS via physical2 node (it requires sftnight password)
```

The version update in lcgcmake still needs to be done manually.

### Add new package

To add new you need to add following lines to usually `external/CMakeLists.txt` or `pyexternal/CMakeList.txt`.

```
LCGPackage_Add(
  <pkgname>
  URL ${GenURL}/<pkgname>-${<pkgname>_native_version}.tar.gz
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR>
  BUILD_COMMAND  ${MAKE}
  INSTALL_COMMAND ${MAKE} install
  BUILD_IN_SOURCE 1
)
```

The next step is to add the following line to all the toolchains that the package should appear.
All toolchains are inside `cmake/toolchain/heptools-*.cmake`.

```
LCG_external_package(<pkgname <pkgversion>)
```

### Upgrade package

If the sources are already installed into EOS, the only thing to do is update the right package inside `cmake/toolchain/heptools-*.cmake`.

Tips&tricks
```
cd lcgcmake/cmake/toolchain
# Upgrade package in all important toolchains
sed -i "s/\(<pkgname> .* *\) \([0-9a-z_]\+[\.\-]\?\)\+/\1 <new_version>/" heptools-{dev3,dev4,experimental}{,python3}.cmake 
# Upgrade package only in python3 toolchains
sed -i "s/\(.*nxcals.* *\) \([0-9a-z_]\+[\.\-]\?\)\+/\1 0.1.101/" heptools-{dev3,dev4,experimental}python3.cmake 
```

