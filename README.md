# LCG Documentation for SPI related activities

Welcome to the documentation of the SPI team. We are part of the SFT group in the EP department at CERN.
This documentation serves multiple purposes:

* Introduce our **users** to the *LCG Releases* and help them getting started
* Document *LCG CMake* **internals** for ourselves and future colleagues
* Describe the **operations** such as common tasks related to VMs, Puppet, Docker, CVMFS etc.

We use [MkDocs](https://www.mkdocs.org/) as an underlying engine to turn a collection of [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) files into a static documentation website.
It is easy to use and recommended by the [MAlt team at CERN](https://malt.web.cern.ch/malt/global/malt-table/).
As a theme we use [*Material for MkDocs*](https://squidfunk.github.io/mkdocs-material/) because it is well-maintained and it comes with a prebuilt Docker image.

The configuration file for `MkDocs` is `mkdocs.yml`. The only part in the configuration that needs to be updated can be found at the bottom of said file in the `nav` section where the navigation for the website is defined. The configuration options of `MkDocs` are documented [here](https://www.mkdocs.org/user-guide/configuration/).


* [Introduction to LCG Releases for end users](docs/lcgreleases/introduction.md)

### LCG machine management

* [Openstack](docs/infrastructure/openstack.md)
* [Puppet configuration](docs/infrastructure/puppet.md)
* [Dockers in LCG](docs/infrastructure/dockers.md)
* [Adding hosts to jenkins infrastructure](docs/infrastructure/newhosts.md)
* [Adding CERN user accounts](docs/infrastructure/newcernuser.md)
* [Adding new system packages to Docker and Puppet](docs/tasks/newpackages.md)
* [Setting up MacOS nodes](docs/infrastructure/macnodes.md)

### Common LCG tasks

* [Copy to EOS](docs/tasks/copytoeos.md)
* [Cleaning a workspace](docs/tasks/cleanup.md)
* [Install a new `gcc` compiler](docs/tasks/installgcc.md)
* [A collection of CVMFS maintenance jobs](docs/tasks/cvmfsmaintenance.md)

### SPI Shift

* [Assignments](docs/shift/assignments.md)
* [Checklist](docs/shift/checklist.md)


## Writing documentation

If you are just updating an existing page, you can use a shortcut and click the pencil button on the top of every documentation page.

For **new documentation pages**, you'll not only have to create the corresponding folders and files but also update the navigation at the bottom of the `mkdocs.yml` configuration file.
The same configuration changes need to be done whenever you **rename** an existing Markdown file.

For **internal links** to other pages in the documentation, please use the relative filename such as `[Assignments](shift/assignments.md)` for example. If your page is in the same subfolder, only use the filename without the folder.

### Extensions

The theme *Material for MkDocs* comes with many extensions.
These allow for advanced features in the Markdown files such as

* [Admonition](https://squidfunk.github.io/mkdocs-material/extensions/admonition/) - Adds block-styled side content to your documentation, for example summaries, notes, hints or warnings. ![Admonition](docs/images/Admonition_block.png "One of many possible Admonition blocks")
* [CodeHilite](https://squidfunk.github.io/mkdocs-material/extensions/codehilite/) - Adds syntax highlighting to code blocks. ![CodeHilite](docs/images/CodeHilite.png "Syntax highlighting")
* [Footnotes](https://squidfunk.github.io/mkdocs-material/extensions/footnotes/) - Adds the ability to add footnotes. ![Footnotes](docs/images/Footnotes.png "What footnotes will look like on page")
* [Metadata](https://squidfunk.github.io/mkdocs-material/extensions/metadata/) - Adds metadata to a document. ![Metadata image](docs/images/Metadata_markdown.png "Written to beginning of document")
* [Permalinks](https://squidfunk.github.io/mkdocs-material/extensions/permalinks/) - Adds anchors at the end of headlines, which makes it possible to directly link to subpart of the document.
* [PyMdown](https://squidfunk.github.io/mkdocs-material/extensions/pymdown/) - Adds a collection of useful Markdown extensions which contain equations, tasklists and more.


## Build process

In order to simplify the build process we encapsulate the build environment in a Docker image called `squidfunk/mkdocs-material` provided by Martin Donath on [DockerHub](https://hub.docker.com/r/squidfunk/mkdocs-material) and [GitHub](https://github.com/squidfunk/mkdocs-material).
Documentation is [here](https://squidfunk.github.io/mkdocs-material/).

The build process boils down to run this command in the `lcgdocs` directory:

```bash
docker run --rm --volume=${PWD}:/docs squidfunk/mkdocs-material build
```

This commands creates or updates the `site` subfolder which contains the HTML, JavaScript, CSS and images that need to be deployed to a web server in the next step.

### Local development

The following command, if run in the `lcgdocs` directory, provides a local web server for testing including an automatic rebuild whenever the Markdown files change:

```bash
docker run --rm --volume ${PWD}:/docs --publish 80:8000 squidfunk/mkdocs-material
```

The result is available at [http://localhost/](http://localhost/).


## Deployment to EOS

The website is hosted using the EOS area `/eos/project/l/lcgdocs/www` and is available at [https://cern.ch/lcgdocs](https://cern.ch/lcgdocs).

The deployment to EOS is triggered by commits to the master branch of this repository with gitlab CI.

## Nightly reporter

Independent from the rest of the documentation, we also build the *Nightly Reporter* in a different repository.
The *Nightly Reporter* is [a static HTML page](https://lcgdocs.web.cern.ch/lcgdocs/report/) representing the current state of our nightly build jobs in Jenkins.
It is refreshed every 30 minutes from 6 A.M. until 8 P.M.

The source code is available on [GitLab](https://gitlab.cern.ch/sft/nightly-reporter), the Jenkins job is [here](https://epsft-jenkins.cern.ch/job/lcg_nightly_reporter).

For convenience (because we didn't want to setup an additional web server), we place the generated HTML plus its CSS and images in a subfolder of the `lcgdocs` area on EOS.


## Alternatives

The CVMFS team uses [Read the Docs](https://docs.readthedocs.io/en/stable/index.html) and its underlying engine [Sphinx](https://www.sphinx-doc.org/en/master/intro.html) to generate the [CVMFS documentation](https://cvmfs.readthedocs.io/en/stable/) based on the [GitHub repository `cvmfs/doc-cvmfs`](https://github.com/cvmfs/doc-cvmfs).
Nowadays, it should work with Markdown as well but initially Sphinx was focused on files in the [reStructuredText format](http://docutils.sourceforge.net/rst.html).
